# Satellite Control Systems and Auxillary Asteriod Mining Unit Embedded Systems
### University of Washington ECE 474 "Intro to Embedded Systems" Final Project and Report

```
This Project used an Arduino Uno and an Arduino Mega to meet the specifications of a theoretical
satellite control system as well as an auxillary asteroid mining unit.

The subsystems include the following:

Battery Temperature Monitor
Console Display
Self Destruct Protocol
Image Capture Surveillance
Pirate/Intruder Distance Calculator
Satellite Communications
Solar Panel Control
Thruster Control
Transport Distance Calculator
Mining Unit Communications
Warning Alarm
Earth Command Reception
Task Queue
```
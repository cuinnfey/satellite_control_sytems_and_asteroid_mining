/*******************************************************
* vehicleComms.cpp
* Authors: Khang Phan
* Assignment 3
* 10/14/18
* This program handles the communication between Earth
* and mining/transport vehicle.
*********************************************************/
#include "vehicleComms.h"

#define MIN_TRANSPORT_DIST 100

/******************************************
* function name:        newVehicleCommsSubsystemTask
* function inputs:      none
* function outputs:     new TCB for vehicleComms
* function description: create an vehicleComms
*           TCB with its dataPtr initialized.
* author:               Khang Phan
******************************************/
TCB newVehicleCommsSubsystemTask() {
  TCB emptyTCB;

  static struct vehicleCommsDataStruct vehicleCommsData;

  vehicleCommsData.commandPtr = &command;
  vehicleCommsData.responsePtr = &response;
  vehicleCommsData.commandFromTransportPtr = &commandFromTransport;
  vehicleCommsData.responseToTransportPtr = &responseToTransport;

  emptyTCB.taskPtr = (void (*)(void*)) vehicleCommsFunc;
  emptyTCB.taskDataPtr = &vehicleCommsData;
  emptyTCB.priority = 3;
  return emptyTCB;
}

/************************************************************
* function name:        vehicleCommsFunc
* function inputs:      struct vehicleCommsDataStruct *dataPtr
* function outputs:     none
* function description: main function of vehicles comms TCB,
*           Read command and send corres ponding response
* author:               Cuinn Fey
*************************************************************/
void vehicleCommsFunc(struct vehicleCommsDataStruct *dataPtr)
{
  char command = *(dataPtr->commandPtr);
  if (command == 'F' || command == 'B'
      || command == 'L' || command == 'R'
      || command == 'D' || command == 'H'
      || command == 'S' || command == 'I' ) {
    VEHICLE_SERIAL.write(command);
    VEHICLE_SERIAL.write('\0');
  }

  char transportCommand = *(dataPtr->commandFromTransportPtr);
  if (transportCommand == 'T') {
    *(dataPtr->responseToTransportPtr) = 'K';
  } else if (transportCommand == 'D') {
    *(dataPtr->responseToTransportPtr) = 'C';
  }

  if (*(dataPtr->responseToTransportPtr) != NULL) {
    VEHICLE_SERIAL.write(*(dataPtr->responseToTransportPtr));
    VEHICLE_SERIAL.write('\0');
    *(dataPtr->commandFromTransportPtr) = NULL;
    *(dataPtr->responseToTransportPtr) = NULL;
  }
  return;
}

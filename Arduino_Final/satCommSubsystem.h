/*******************************************************
  satCommSubsystem.h
  Authors: John McIntire
  Assignment 2
  10/14/18
  This is header of satCommSubsystem.c, containing the data
  struct used by consoleDisplay, also the seed and maximum
  integer value used by random
*********************************************************/
#ifndef SAT_COM_SUBSYS_H
#define SAT_COM_SUBSYS_H

#include "Arduino_Final.h"
#include "globalVariableInitialization.h"
#include "TCBStruct.h"

struct satCommSubsystemDataStruct {
  unsigned int *thrusterCommandPtr;
  Bool *solarPanelStatePtr, *fuelLowPtr, *batteryLowPtr;
  unsigned short *batteryLevelPtr, *fuelLevelPtr, *powerConsumptionPtr, *powerGenerationPtr;
  struct TransportDistancePointerStruct *transportDistancePointerPtr;
  struct BatteryTemperaturePointerStruct *batteryTemperaturePointerPtr;
  struct ImageBufferStruct *imageBufferPtr;
};

void satCommSubsystemFunc (struct satCommSubsystemDataStruct *dataPtr);
void sendData();
void receiveData();
void remoteDisplayUpdate(struct satCommSubsystemDataStruct *dataPtr);

TCB newSatCommSubsystemTask();


#endif

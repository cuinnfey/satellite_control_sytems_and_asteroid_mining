/*******************************************************
* batteryTemperature.h
* Authors: Khang Phan
* Assignment 4
* 11/15/18
* This is header of batteryTemperature.c, containing the data
* struct used by battery temperature measurement
*********************************************************/
#ifndef BATT_TEMP_H
#define BATT_TEMP_H

#include <Arduino.h>
#ifdef __cplusplus
extern "C" {
#endif

#include "globalVariableInitialization.h"
#include "TCBStruct.h"


struct batteryTempStruct {
    struct BatteryTemperaturePointerStruct *batteryTemperaturePointerPtr;
};

TCB newBatteryTempTask();
void batteryTempFunc (struct batteryTempStruct *dataPtr);
unsigned int millivoltToCelcius(unsigned int millivolt);
#ifdef __cplusplus
} // extern "C"
#endif
#endif

/*******************************************************
  destructSubsystem.c
  Authors: John McIntyre
  Assignment 3
  12/09/18time
  This program handles the final security protocal a
  self destruction if the alien is closer then 3 meters
  This can be overrided if the correct code is entered and
  a button is pressed
*********************************************************/
#include "destructSubsystem.h"

/*********************************************************
  function name:        newDestructSubsystemTask
  function inputs:      none
  function outputs:     new TCB for destructSubsystem
  function description: create an self destruction subsystem TCB with
             its dataPtr initialized.
  author:               John McIntyre
***********************************************************/
TCB newDestructSubsystemTask() {
  TCB emptyTCB;

  static struct destructSubsystemDataStruct destructSubsystemData;

  destructSubsystemData.timeToDestructPtr = &timeToDestruct;

  emptyTCB.taskPtr = (void*) destructSubsystemFunc;
  emptyTCB.taskDataPtr = &destructSubsystemData;
  emptyTCB.priority = 4;
  return emptyTCB;
}

/************************************************************
  function name:        destructSubsystemFunc
  function inputs:      struct destructSubsystemDataStruct *dataPtr
  function outputs:     none
  function description: main function of self destruction subsystem TCB,
            prints the time to self destruct and decrements time to
            self destruction. If destruct protocol fails then enacts
			satellite destruction
  author:               John McIntyre
*************************************************************/
void destructSubsystemFunc(struct destructSubsystemDataStruct *dataPtr) {

  //conditional that will print and display self - destruction
  //information
  if (0 < (*(dataPtr->timeToDestructPtr))) {
    printDestruct();
    flashScreen();
    decreTimeToDestruct();
    //enact destruction protocol
  } else {
    destructProtocol();
  }
}


//reset function for destruction
void(* resetFunc)(void) = 0;
/************************************************************
  function name:        destructProtocal
  function inputs:      void
  function outputs:     none
  function description: function that enacts the destruction of
			satellite: reset memory signal explosive
			satellite destruction
  author:               John McIntyre
*************************************************************/
void destructProtocol(void) {
  //send signal to explosives and reset system
  digitalWrite(DESTRUCT_PIN, HIGH);
  resetFunc(); 						//may never get to
}

/************************************************************
  function name:        flashScreen
  function inputs:      void
  function outputs:     none
  function description: Function display and flash the current time
 						to self destruction
  author:               John McIntyre
*************************************************************/
void flashScreen() {
  static int prevDestState = -1;

  if ((millisec - destructFlashTimer) <= FLASH1 && prevDestState != 1) {
    tft.fillRect(0, 170, 80, 45, BLACK);
    analogWrite(SIREN, SIREN_ON);
    prevDestState = 1;
  } else if ((millisec - destructFlashTimer) <= FLASH2 && prevDestState != 2) {
    tft.setTextColor(RED);
    tft.setCursor(3, 180);
    tft.setTextSize(4);
    tft.print(timeToDestruct);
    prevDestState = 2;
  } else if ((millisec - destructFlashTimer) <= FLASH3 && prevDestState != 3) {
    tft.fillRect(0, 170, 80, 45, BLACK);
    analogWrite(SIREN, SIREN_OFF);
    prevDestState = 3;
  } else {
    destructFlashTimer = millisec;
    prevDestState = -1;
  }
}

/************************************************************
  function name:        decrTimeToDestruct
  function inputs:      void
  function outputs:     none
  function description: Function to decrement the seconds to
						self destruction by one each second
  author:               John McIntyre
*************************************************************/
void decreTimeToDestruct() {
  if ((millisec - destructCounterTimer) >= SECOND) {
    timeToDestruct--;
    destructCounterTimer = millisec;
  }
}

extern Bool needClear;
/************************************************************
  function name:        printDestruct
  function inputs:      void
  function outputs:     none
  function description: Function to print that the self destruct
            sequnce has started
  author:               John McIntyre
*************************************************************/
void printDestruct() {
  if (needClear == FALSE) {
    //setting the details for the text to display
    tft.setTextColor(RED);
    tft.setTextSize(2);
    tft.setCursor(0, 115);

    //printing of destruction warning
    tft.println("Self");
    tft.println("Destruction");
    tft.println("in: ");
  }
}

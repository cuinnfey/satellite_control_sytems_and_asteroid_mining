/*******************************************************
* transportDistance.h
* Authors: Khang Phan
* Assignment 4
* 11/15/18
* This is header of transportDistance.c, containing the data
* struct used by transportDistance
*********************************************************/
#ifndef TRANSPORT_DISTANCE__H
#define TRANSPORT_DISTANCE__H

#include <Arduino.h>

#include "globalVariableInitialization.h"
#include "TCBStruct.h"


struct transportDistanceDataStruct {
    struct TransportDistancePointerStruct *transportDistancePointerPtr;
};

TCB newTransportDistanceTask();
void transportDistanceFunc (struct transportDistanceDataStruct *dataPtr);
int getDistanceMeter();
#endif

/*******************************************************
  warningAlarm.cpp
  Authors: Khang Phan, John McIntyre
  Assignment 2
  10/14/18 update 11/19/2018
  This program print on the TFT the annunciation "FUEL"
  and "BATTERY", blinking and changing color as required by
  the specification
*********************************************************/
#include "warningAlarm.h"


//constants for Tempurature timing
const unsigned int CYCLE_TIME = 5000;
const unsigned int FLASH_CYCLE = 50;
const unsigned int SOLID_CONST = 15000;

//timing variables for Tempurature warning
unsigned int currentTempTime;
unsigned int startTempTime;
unsigned int startTempFlash;
unsigned int startTempWarning;

/*********************************************************
  function name:        newWarningAlarmTask
  function inputs:      none
  function outputs:     new TCB for warningAlarm
  function description: create an warning alarm TCB with
             its dataPtr initialized.
  author:               Khang Phan, John McIntyre
***********************************************************/
TCB newWarningAlarmTask() {
  TCB emptyTCB;

  static struct warningAlarmDataStruct warningAlarmData;

  warningAlarmData.fuelLowPtr = &fuelLow;
  warningAlarmData.batteryLowPtr = &batteryLow;
  warningAlarmData.batteryLevelPtr = &batteryLevel;
  warningAlarmData.fuelLevelPtr = &fuelLevel;
  warningAlarmData.batteryOverTemperaturePtr = &batteryOverTemperature;

  emptyTCB.taskPtr = (void (*)(void*)) warningAlarmFunc;
  emptyTCB.taskDataPtr = &warningAlarmData;
  emptyTCB.priority = 5;
  return emptyTCB;
}

/************************************************************
  function name:        warningAlarmFunc
  function inputs:      struct warningAlarmDataStruct *dataPtr
  function outputs:     none
  function description: main function of warning alarm TCB,
                manage timing and display/blinking the
                FUEL and BATTERY on TFT screen
  author:               Khang Phan, John McIntyre
*************************************************************/
void warningAlarmFunc(struct warningAlarmDataStruct *dataPtr) {

  // Note that the states described in battery and fuel display is
  // as following:
  //    0 - clear
  //    1 - green
  //    2 - orange
  //    3 - red

  // BATTERY WARNING
  static int battPrevState = 0;                 // Previous state that appearing
  // on he TFT

  static unsigned long battClock = 0;           // Starting point clock used to
  // blink battery

  int battNewState = battPrevState;             // Initialize new state to previous
  // state

  tft.setTextSize(4);                           // Set text size to 4

  if (*(dataPtr->batteryLevelPtr) > 50) {
    // Battery > 50%, set to green
    battNewState = 1;
  } else if (*(dataPtr->batteryLevelPtr) > 10) {
    // 10% < Battery <= 50%
    if (battPrevState == 2) {
      if (millisec - battClock > 1000) {
        // If it was orange for 1 sec, clear it
        battNewState = 0;
        battClock = millisec;
      }
    } else if (battPrevState != 0 ||
               (battPrevState == 0 && millisec - battClock > 1000)) {
      // If it was red or green, change to orange immediately.
      // If it was cleared for 1 sec, change it to orange
      battNewState = 2;
      battClock = millisec;
    }
  } else if (battPrevState == 3) {
    // Battery <= 10%
    if (millisec - battClock > 1000) {
      // If it was red for 1 sec, clear it
      battNewState = 0;
      battClock = millisec;
    }
  } else if (battPrevState != 0 ||
             (battPrevState == 0 && millisec - battClock > 1000)) {
    // If it was green or orange, change to red immediately.
    // If it was cleared for 1 sec, change it to red
    battNewState = 3;
    battClock = millisec;
  }

  if (battPrevState != battNewState) {
    // State was changed from last state, draw Battery
    // annunciation on the TFT
    printBattery(battNewState);
    // Update previous state
    battPrevState = battNewState;
    if (battNewState != 0) {
      if (battNewState == 3) {
        // Set batteryLow to true if battery go under 10%
        *(dataPtr->batteryLowPtr) = TRUE;
      } else {
        // Set batteryLow to false if battery go above 10%
        *(dataPtr->batteryLowPtr) = FALSE;
      }
    }
  }


  //FUEL WARNING!!!!!
  static int fuelPrevState = 0;                 // Previous state that appearing
  // on he TFT

  static unsigned long fuelClock = 0;           // Starting point clock used to
  // blink battery

  int fuelNewState = fuelPrevState;             // Initialize new state to previous
  // state

  if (*(dataPtr->fuelLevelPtr) > 50) {
    // Fuel > 50%, set to green
    fuelNewState = 1;
  } else if (*(dataPtr->fuelLevelPtr) > 10) {
    // 10% <Fuel <= 50%
    if (fuelPrevState == 2) {
      if (millisec - fuelClock > 2000) {
        // If it was orange for 2 sec, clear it
        fuelNewState = 0;
        fuelClock = millisec;
      }
    } else if (fuelPrevState != 0 ||
               (fuelPrevState == 0 && millisec - fuelClock > 2000)) {
      // If it was red or green, change to orange immediately.
      // If it was cleared for 2 sec, change it to orange
      fuelNewState = 2;
      fuelClock = millisec;
    }
  } else if (fuelPrevState == 3) {
    // Fuel <= 10%
    if (millisec - fuelClock > 2000) {
      // If it was red for 2 sec, clear it
      fuelNewState = 0;
      fuelClock = millisec;
    }
  } else if (fuelPrevState != 0 ||
             (fuelPrevState == 0 && millisec - fuelClock > 2000)) {
    // If it was red or green, change to red immediately.
    // If it was cleared for 2 sec, change it to red
    fuelNewState = 3;
    fuelClock = millisec;
  }

  if (fuelPrevState != fuelNewState) {
    // State was changed from last state, draw Battery
    // annunciation on the TFT
    printFuel(fuelNewState);
    // Update previous state
    fuelPrevState = fuelNewState;
    if (fuelNewState != 0) {
      if (fuelNewState == 3) {
        // Set fuelLow to true if Fuel go under 10%
        *(dataPtr->fuelLowPtr) = TRUE;
      } else {
        // Set fuelLow to false if Fuel go above 10%
        *(dataPtr->fuelLowPtr) = FALSE;
      }
    }
  }

  //TEMPURATE WARNING!!!!!
  //if the battery tempurature is too high
  static int tempPrevState = 0;
  int tempNewState = tempPrevState;
  if (TRUE == *(dataPtr->batteryOverTemperaturePtr)) {
    //check if in 15 second window
    if ((currentTempTime - startTempTime) <= SOLID_CONST) {
      tempNewState = 1;
      currentTempTime = startTempWarning = millisec;    //update timing variables
    } else if (FALSE == tempAcknowledge) {
      // if no acknowlwdgememnt go into hyper notification
      if ((currentTempTime - startTempWarning) <= CYCLE_TIME) {
        //5 sec solid  first half of cycle
        tempNewState = 1;
        currentTempTime = startTempFlash = millisec;    //update timing variables
      } else if ((currentTempTime - startTempWarning) <= (2 * CYCLE_TIME)) {
        // enter 5 sec flashing second half of cycle
        if ((currentTempTime - startTempFlash) <= FLASH_CYCLE) {
          tempNewState = 0;
          currentTempTime = millisec;           //update timing variables
        } else if ((currentTempTime - startTempFlash) <= (2 * FLASH_CYCLE)) {
          tempNewState = 1;
          currentTempTime = millisec;           //update timing variables
        } else {
          currentTempTime = startTempFlash = millisec;    //update timing variables
        }
      } else {
        currentTempTime = startTempWarning = millisec;    //update timing variables
      }
    }
  } else {
    //the reset of the variables nd screen based on the assupmtion system will not
    // start in battery temp over state
    tempNewState = 0;
    tempAcknowledge = FALSE;
    currentTempTime = startTempTime = millisec;
  }

  if (tempNewState != tempPrevState) {
    printTemp(tempNewState);
    tempPrevState = tempNewState;
  }
  return;
}

/************************************************************
  function name:        printBattery
  function inputs:      int state
  function outputs:     none
  function description: print BATTERY on TFT based on the state:
                    0 - not display BATTERY
                    1 - BATTERY in green
                    2 - BATTERY in orange
                    3 - BATTERY in red
  author:               Khang Phan
*************************************************************/
void printBattery(int state) {
  // Move cursor to destinate position
  tft.setCursor(0, 0);
  if (state == 0) {
    // Remove the word by filling with background color
    tft.fillRect(0, 0, 175, 30, BLACK);
  } else if (state == 1) {
    tft.setTextColor(GREEN);
    tft.print("BATTERY");
  } else if (state == 2) {
    tft.setTextColor(ORANGE);
    tft.print("BATTERY");
  } else if (state == 3) {
    tft.setTextColor(RED);
    tft.print("BATTERY");
  }

  return;
}

/************************************************************
  function name:        printFuel
  function inputs:      int state
  function outputs:     none
  function description: print FUEL on TFT based on the state:
                    0 - not display FUEL
                    1 - FUEL in green
                    2 - FUEL in orange
                    3 - FUEL in red
  author:               Khang Phan
*************************************************************/
void printFuel(int state) {
  // Move cursor to destinate position
  tft.setCursor(0, 40);
  if (state == 0) {
    // Remove the word by filling with background color
    tft.fillRect(0, 40, 100, 30, BLACK);
  } else if (state == 1) {
    tft.setTextColor(GREEN);
    tft.print("FUEL");
  } else if (state == 2) {
    tft.setTextColor(ORANGE);
    tft.print("FUEL");
  } else if (state == 3) {
    tft.setTextColor(RED);
    tft.print("FUEL");
  }

  return;
}

/************************************************************
  function name:        printTemp
  function inputs:      int state
  function outputs:     none
  function description: print TEMPURATURE on TFT based on the state:
                    0 - No Display of Tempurature Warning
                    1 - TEMPURATURE displayed in RED
                    2 - TEMPURATURE in BLACK in RED BOX - for flash effect
                    3 - TEMPURATURE in RED in BLACK BOX - for flash effect
  author:               John McIntyre
*************************************************************/
void printTemp(int state) {
  // Move cursor to destinate position
  tft.setCursor(0, 80);

  //Selection of manner to display Warning - see above
  switch ( state) {
    case 0:
      tft.fillRect(0, 80, 270, 70, BLACK);
      break;
    case 1:
      tft.setTextColor(RED);
      tft.print("TEMPURA");
      tft.setCursor(40, 120);
      tft.print("-TURE");
      break;
  }

  return;
}

/*******************************************************
* imageCapture.h
* Authors: Cuinn Fey
* Assignment 4
* 11/16/18
* 
*********************************************************/
#ifndef IMAGE_CAPTURE_H
#define IMAGE_CAPTURE_H


#include "globalVariableInitialization.h"
#include "TCBStruct.h"
#include "Arduino_Final.h"

struct imageCaptureDataStruct {
  int* vRealPtr;
	int* vImagPtr;
};

void imageCaptureFunc(struct imageCaptureDataStruct *dataPtr);
TCB newImageCaptureSubsystemTask();


#endif

/*******************************************************
* solarPanelControl.c
* Authors: John McIntyre
* Assignment 3
* 11/2/18
* This program handles console control of the deployment
* and retraction of the Solar panel during cahring
* of the battery
*********************************************************/
#define CHANGE_CONST 5
#define MAX_RATE 100
#define MIN_RATE 0
#include "solarPanelControl.h"

/*********************************************************
* function name:        newSolarPanelControlTask
* function inputs:      none
* function outputs:     new TCB for satCommSubsystem
* function description: create a solar panel control TCB with
*            its dataPtr initialized.
* author:               John McIntyre
***********************************************************/
TCB newSolarPanelControlTask() {
    TCB emptyTCB;

    static struct solarPanelControlStruct solarPanelControlData;
    
    solarPanelControlData.solarPanelStatePtr = &solarPanelState;
    solarPanelControlData.solarPanelDeployPtr = &solarPanelDeploy;
    solarPanelControlData.solarPanelRetractPtr = &solarPanelRetract;
    solarPanelControlData.driveMotorSpeedIncPtr = &driveMotorSpeedInc;
    solarPanelControlData.driveMotorSpeedDecPtr = &driveMotorSpeedDec;
    
    emptyTCB.taskPtr = (void*) solarPanelControlFunc;
    emptyTCB.taskDataPtr = &solarPanelControlData;
    return emptyTCB;
}

/************************************************************
* function name:        solarPanelControlFunc
* function inputs:      struct solarPanelControlDataStruct *dataPtr
* function outputs:     none
* function description: main function of the solar panel control
*           subsystem TCB, sends a PWM to the solar panel drive
*            motor and will increase / descrease speed based on
*            console input
* author:               John McIntyre
*************************************************************/
void solarPanelControlFunc(struct solarPanelControlStruct *dataPtr){

    //if the console subsystem has set either the increase or
    //decrease falg to true of false the break point for the
    //solar panel PWM is increased or decreased
    if (TRUE == *(dataPtr->driveMotorSpeedIncPtr)){
        if (curr_rate < MAX_RATE){
          curr_rate += CHANGE_CONST;
        }
        *(dataPtr->driveMotorSpeedIncPtr) = FALSE;           //reset of increase flag
        solarPWMPulsePoint = curr_rate*PULSE_PERIOD/MAX_RATE;//update pulse break
    } else if (TRUE == *(dataPtr->driveMotorSpeedDecPtr)){
        if (curr_rate > MIN_RATE){
          curr_rate -= CHANGE_CONST;
        }
        *(dataPtr->driveMotorSpeedDecPtr) = FALSE;        	  //reset of decrease flag
        solarPWMPulsePoint = curr_rate*PULSE_PERIOD/MAX_RATE; //update pulse break     }
}

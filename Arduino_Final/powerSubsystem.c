/*******************************************************
* powerSubsystem.c
* Authors: Khang Phan
* Assignment 2
* 10/14/18
* This program handles the power being consumed, generated
* and result power in the battery, also control solar panel
* if needed
*********************************************************/
#include "powerSubsystem.h"


#define DEPLOY_POINT 95
#define ReTRACT_POINT 10
#define WAIT_TIME_BATTERY_READ 600
#define MAX_BATTERY_READ 1023
#define HUNDRED 100

/*********************************************************
* function name:        newPowerSubsystemTask
* function inputs:      none
* function outputs:     new TCB for powerSubsystem
* function description: create an power subsystem TCB with
*            its dataPtr initialized.
* author:               Khang Phan
***********************************************************/
TCB newPowerSubsystemTask() {
    TCB emptyTCB;

    static struct powerSubsystemDataStruct powerSubsystemData;

    powerSubsystemData.batteryLevelPtr = &batteryLevel;
    powerSubsystemData.powerConsumptionPtr = &powerConsumption;
    powerSubsystemData.powerGenerationPtr = &powerGeneration;
    powerSubsystemData.solarPanelStatePtr = &solarPanelState;
    powerSubsystemData.solarPanelDeployPtr = &solarPanelDeploy;
    powerSubsystemData.solarPanelRetractPtr = &solarPanelRetract;
    powerSubsystemData.batteryPointerPtr = &batteryPointer;
    emptyTCB.taskPtr = (void*) powerSubsystemFunc;
    emptyTCB.taskDataPtr = &powerSubsystemData;
    emptyTCB.priority = 3;
    return emptyTCB;
}

/************************************************************
* function name:        powerSubsystemFunc
* function inputs:      struct powerSubsystemDataStruct *dataPtr
* function outputs:     none
* function description: main function of power subsystem TCB,
*           adjust power consumption, generation and battery
*           level
* author:               Khang Phan
*************************************************************/
void powerSubsystemFunc (struct powerSubsystemDataStruct *dataPtr) {
    static Bool evenCalled = TRUE;      // True if this is the even time
                                        // function is called
    // Description of these function is bellow.
    updatePowerConsumption(dataPtr->powerConsumptionPtr, evenCalled);

    updatePowerGen(dataPtr->powerGenerationPtr, dataPtr->batteryLevelPtr,
                   dataPtr->solarPanelStatePtr, evenCalled);

    readBattery();
    // Update for next call
    evenCalled = !evenCalled;

    if (batteryLevel > 95 && (*(dataPtr->solarPanelStatePtr))) {
      *(dataPtr->solarPanelRetractPtr) = TRUE;
      *(dataPtr->solarPanelDeployPtr) = FALSE;
      solarPWMCounter = 0;
      *(dataPtr->powerGenerationPtr) = 0;      
    } else if (batteryLevel < 10 && !(*(dataPtr->solarPanelStatePtr))) {
      *(dataPtr->solarPanelRetractPtr) = FALSE;
      *(dataPtr->solarPanelDeployPtr) = TRUE;
      solarPWMCounter = 0;
    }
    return;
}

/************************************************************
* function name:        updatePowerConsumption
* function inputs:      unsigned short *powerConsPtr
*                       Bool evenCalled
* function outputs:     none
* function description: adjust power consumption based on the
*                specification
* author:               Khang Phan
*************************************************************/
void updatePowerConsumption (unsigned short *powerConsPtr, Bool evenCalled){
    static short passedTen = FALSE;     // false if consumption is
                                        // increasing and true otherwise
    if (!passedTen) {
        // Based on the call is even or not, adjust power consumption,
        // increasing by 2 on even and decreasing by 1 on odd
        if (evenCalled) {
            (*powerConsPtr) += 2;
        } else {
            (*powerConsPtr) -= 1;
        }

        // Update if power consumption goes pass 10
        if ((*powerConsPtr) > 10) {
            passedTen = !passedTen;
        }
    } else {
        // Based on the call is even or not, adjust power consumption,
        // decreasing by 2 on even and increasing by 1 on odd
        if (evenCalled) {
            (*powerConsPtr) -= 2;
        } else {
            (*powerConsPtr) += 1;
        }

        // Update if power consumption goes below 5
        if ((*powerConsPtr) < 5) {
            passedTen = !passedTen;
        }
    }

    return;
}

/************************************************************
* function name:        updatePowerGen
* function inputs:      unsigned short *powerGenPtr
*                       unsigned short *battLevelPtr
*                       Bool *solarPanStatPtr
*                       Bool evenCalled
* function outputs:     none
* function description: adjust power generation, retract
*               and deploy solar panel based on requirement
* author:               Khang Phan
*************************************************************/
void updatePowerGen (unsigned short *powerGenPtr, unsigned short *battLevelPtr,
                      Bool *solarPanStatPtr, Bool evenCalled) {
    if (*solarPanStatPtr) {
       if (evenCalled) {
         // Increase power generation by 2 on even called
         *powerGenPtr += 2;
       } else {
         // Increase power generation by 1 on odd called
         // if power is less or equal to 50%
         if (*battLevelPtr <= 50) {
           *powerGenPtr += 1;
         }
      }
    } 
    return;
}

/************************************************************
* function name:        readBattery
* function inputs:      none
* function outputs:     none
* function description: read battery from outside with
*             analog read
* author:               Khang Phan
*************************************************************/
void readBattery() {
  // pin 34 used to test the delay 600us (WAIT_TIME_BATTERY_READ)
  // before battery read
  //pinMode(34,OUTPUT);
  connectMeasureEquipment();
  //digitalWrite(34, HIGH);
  delayMicroseconds(WAIT_TIME_BATTERY_READ);
  //digitalWrite(34, LOW);
  measureBatteryInterrupt = TRUE;

  // delay 2mm does not stop interrupt, called to make sure 
  // reading battery interruption is done before continue
  delay(2);
  disconnectMeasureEquipment();
}

/************************************************************
* function name:        readBatteryInterruptFunction
* function inputs:      none
* function outputs:     none
* function description: during interruption, this function
*           is called to actually do the measuring of
*           battery and saved it to the circular buffer
* author:               Khang Phan
*************************************************************/
void readBatteryInterruptFunction() {
  // increase index of buffer front
  batteryPointer.front += 1;
  batteryPointer.front %= 16;

  // read battery level
  batteryPointer.battArray[batteryPointer.front] = analogRead(BATTERY_PIN);

  //update global variable
  realBattLevel = (((double) batteryPointer.battArray[batteryPointer.front] / MAX_BATTERY_READ)*MAX_BATT);
  batteryLevel = (unsigned short) (realBattLevel / MAX_BATT * HUNDRED);
  measureBatteryInterrupt = FALSE;
}
void connectMeasureEquipment() {}
void disconnectMeasureEquipment() {}

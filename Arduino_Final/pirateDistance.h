/*******************************************************
  transportDistance.h
  Authors: Khang Phan
  Assignment 4
  11/15/18
  This is header of transportDistance.c, containing the data
  struct used by transportDistance
*********************************************************/
#ifndef	PIRATEDISTANCE_DISTANCE_H
#define PIRATEDISTANCE_DISTANCE_H

#include <Arduino.h>
//#ifdef __cplusplus
//extern "C" {
//#endif

#include "globalVariableInitialization.h"
#include "TCBStruct.h"


struct pirateDistanceDataStruct {
  int* pirateDistanceMeterPtr;
  Bool* pirateDetectedPtr;
};

TCB newPirateDistanceTask();
void pirateDistanceFunc (struct pirateDistanceDataStruct *dataPtr);
int getDistancePirateMeter();
//#ifdef __cplusplus
//} // extern "C"
//#endif
#endif

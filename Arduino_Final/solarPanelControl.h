/*******************************************************
* solarPanelControl.h
* Authors: John McIntyre
* Assignment 3
* 11/2/2018
* This is header of solarPanelControl.c, containing the data
* struct used by the solar control TCB, also the seed and maximum
* integer value used by random
*********************************************************/

#ifndef SOLPANEL_SUBSYS_H
#define SOLPANEL_SUBSYS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "globalVariableInitialization.h"
#include "TCBStruct.h"

static unsigned int curr_rate = 50;        //default rate at 50

struct solarPanelControlStruct {    
    Bool *solarPanelStatePtr;
    Bool *solarPanelDeployPtr;
    Bool *solarPanelRetractPtr;
    Bool *driveMotorSpeedIncPtr;
    Bool *driveMotorSpeedDecPtr;        
};

void solarPanelControlFunc(struct solarPanelControlStruct *dataPtr);

TCB newSolarPanelControlTask();
#ifdef __cplusplus
} // extern "C"
#endif

#endif

/*******************************************************
  vehicleComms.h
  Authors: Cuinn Fey
  Assignment 3
  10/25/18
*********************************************************/
#ifndef VEHICLE_COMMS_H
#define VEHICLE_COMMS_H

#include "globalVariableInitialization.h"
#include "TCBStruct.h"
#include "Arduino_Final.h"

struct vehicleCommsDataStruct {
  char* commandPtr;
  char* responsePtr;
  char *commandFromTransportPtr;
  char *responseToTransportPtr;
};

TCB newVehicleCommsSubsystemTask();
void vehicleCommsFunc(struct vehicleCommsDataStruct *dataPtr);
#endif

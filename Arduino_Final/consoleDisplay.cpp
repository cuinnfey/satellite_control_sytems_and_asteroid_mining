/*******************************************************
* consoleDisplay.cpp
* Authors: Cuinn Fey and Khang Phan
* Assignment 2
* 10/14/18
* This program handles the consoleDisplay output to the
* serial monitor on the Arduino. The program operates
* on two modes switched by a button on the touch screen
* of the TFT display. "Annunication" mode announces the
* fuelLow and batteryLow status. "Satellite Status" mode
* reports important numerical status information. This
* program takes void pointer and returns void.
*********************************************************/
#include "consoleDisplay.h"
extern float actFuel;

/******************************************
* function name:        newConsoleDisplayTask
* function inputs:      none
* function outputs:     new TCB for consoleDisplay
* function description: create an console display
*           TCB with its dataPtr initialized.
* author:               Cuinn Fey
******************************************/
TCB newConsoleDisplayTask() {
    TCB emptyTCB;

    static struct consoleDisplayDataStruct consoleDisplayData;

    consoleDisplayData.fuelLowPtr = &fuelLow;
    consoleDisplayData.batteryLowPtr = &batteryLow;
    consoleDisplayData.solarPanelStatePtr = &solarPanelState;
    consoleDisplayData.batteryLevelPtr = &batteryLevel;
    consoleDisplayData.fuelLevelPtr = &fuelLevel;
    consoleDisplayData.powerConsumptionPtr = &powerConsumption;
    consoleDisplayData.powerGenerationPtr = &powerGeneration;
    consoleDisplayData.transportDistancePointerPtr = &transportDistancePointer;
    consoleDisplayData.batteryTemperaturePointerPtr = &batteryTemperaturePointer;
    consoleDisplayData.imageBufferPtr = &imageBuffer;


    emptyTCB.taskPtr = (void (*)(void*)) consoleDisplayFunc;
    emptyTCB.taskDataPtr = &consoleDisplayData;
    emptyTCB.priority = 3;
    return emptyTCB;
}

/************************************************************
* function name:        consoleDisplayFunc
* function inputs:      struct consoleDisplayDataStruct *dataPtr
* function outputs:     none
* function description: main function of console display TCB
*           depend on the mode either print status or
*           annunciation on the serial
* author:               Cuinn Fey
*************************************************************/
void consoleDisplayFunc(struct consoleDisplayDataStruct *dataPtr) {
   Serial.print("\n\n\n\n\n");               // clear screen
    if (consoleMode == 1) {
        Serial.print("ANNUNCIATION MODE\n");
        Serial.print("FUEL LOW: ");
        if (*(dataPtr->fuelLowPtr)) {
          Serial.print("TRUE\n");
        } else {
           Serial.print("FALSE\n");
        }
        Serial.print("BATTERY LOW: ");
         if (*(dataPtr->batteryLowPtr)) {
          Serial.print("TRUE\n");
        } else {
           Serial.print("FALSE\n");
        }
    } else {
        Serial.println("SATTELITE STATUS MODE");
        if (*(dataPtr->solarPanelStatePtr) == TRUE) {
            Serial.println("Solar Panels: Deployed");
        } else {
            Serial.println("Solar Panels: Retracted");
        }
        Serial.print("Battery Level: ");
        Serial.print(*(dataPtr->batteryLevelPtr));
        Serial.print("% - ");
        Serial.print(realBattLevel,1);       
        Serial.print(" V");
        Serial.println();  
        Serial.print("Fuel Level: ");
        Serial.println(*(dataPtr->fuelLevelPtr));       
        Serial.print("Power consumption: ");
        Serial.println(*(dataPtr->powerConsumptionPtr));
        Serial.print("Power generation: ");
        Serial.println(*(dataPtr->powerGenerationPtr));
        Serial.print("Mining vehicle distance: ");
        Serial.print(dataPtr->transportDistancePointerPtr->transportDistance[dataPtr->transportDistancePointerPtr->front]);    
        Serial.println(" meters");   
        if (solarPanelState == TRUE) {
          Serial.print("Temperature 1: ");
          Serial.print(millivoltToCelcius(dataPtr->batteryTemperaturePointerPtr->temperatureMillivolt1[dataPtr->batteryTemperaturePointerPtr->front]));    
          Serial.println(" C");   
          Serial.print("Temperature 2: ");
          Serial.print(millivoltToCelcius(dataPtr->batteryTemperaturePointerPtr->temperatureMillivolt2[dataPtr->batteryTemperaturePointerPtr->front]));    
          Serial.println(" C");              
        } else {
          Serial.print("Image peak frequency: ");
          Serial.print(dataPtr->imageBufferPtr->bufferArray[imageBuffer.front]);    
          Serial.println(" Hz"); 
        }
        if (pirateDistanceActivated == TRUE) {
          Serial.print("Pirate distance: ");
          Serial.println(pirateDistanceMeter);
        }
    }
    return;
}

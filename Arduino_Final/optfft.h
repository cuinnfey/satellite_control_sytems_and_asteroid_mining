
#ifndef OP_FFT_H
#define OP_FFT_H
#ifdef __cplusplus
extern "C" {
#endif


signed int optfft(signed int x[256], signed int y[256]);
#ifdef __cplusplus
} // extern "C"
#endif

#endif

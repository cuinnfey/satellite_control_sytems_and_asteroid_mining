/*******************************************************
  taskQueue.c
  Authors: Cuinn Fey, John McIntyre
  Assignment 2
  10/14/18 updated 11/19/2018
  This program to manage the addition
  and removal of tasks
*********************************************************/
#include "taskQueue.h"
#define NULL 0

TCB* head = NULL;
TCB* tail = NULL;
TCB* current;

int numberOfTasks = 0;

/*********************************************************
  function name:        insert
  function inputs:      TCB node pointer
  function outputs:     none
  function description: Insert a task into the taskQueue
                        which is implemented as a circular
                        linked list
						- implemented as insert sort
  author:               John McIntyre
***********************************************************/
void insertTask(TCB* node) {
  /*Legacy Code

    if(NULL == head)        // If the head pointer is pointing to nothing
    {
      head = node;         // set the head and tail pointers to point to this node
      tail = node;
    }
    else                     // otherwise, head is not NULL, add the node to the end of the list
    {
      tail -> next = node;
      node -> prev = tail; // note that the tail pointer is still pointing
                           // to the prior last node at this point
      tail = node;         // update the tail pointer
      tail->next = head;
      head->prev = tail;
    }
  */
  Bool notPlaced = TRUE;
  current = head;
  while (notPlaced) {
    if (current == node) {
      return;
    }
    //if the list is empty
    if (NULL == current) {
      //head = (TCB *)malloc(sizeof(TCB));			//allocating memeory for TCB pointer
      //tail = malloc(sizeof(TCB));					//allocating memeory for TCB pointer
      head = node; 								//setting the head to point to node
      tail = node;								//setting the tail to point to node

      notPlaced = FALSE;							//notPlaced to FALSE to exit
    } else {
      //if the new node has lower priority then the head
      if (((node->priority) < current->priority) && (head == current)) {
        //node->next = malloc(sizeof(TCB));		//allocating memeory for TCB pointer
        node->next = current;					//setting node's next
        //current->prev = malloc(sizeof(TCB));	//allocating memeory for TCB pointer
        node->prev = current->prev;
        node->prev->next = node;
        current->prev = node;					//setting reviewed node's prev
        head = node;							//setting the head to point to node

        notPlaced = FALSE;						//notPlaced to FALSE to exit
        //if the in the middle of queue
      } else if (node->priority < current->priority) {
        //node->next = malloc(sizeof(TCB));		//allocating memeory for TCB pointer
        node->next = current;					//setting node's next
        //node->prev = malloc(sizeof(TCB));		//allocating memeory for TCB pointer
        node->prev = current->prev;				//setting node's prev
        current->prev->next = node;				//setting the node that pointed to current to node
        current->prev = node;
        notPlaced = FALSE;						//notPlaced to FALSE to exit
        //if new node is larger then tail
      } else if (current == tail) {
        //node->prev = malloc(sizeof(TCB));		//allocating memeory for TCB pointer
        node->prev = current; 					//setting node's next
        //current->next = malloc(sizeof(TCB));	//allocating memeory for TCB pointer
        node->next = current->next;
        node->next->prev = node;
        current->next = node; 					//setting tail node next's next
        tail = node; 							//setting the tail to point to node

        notPlaced = FALSE;						//notPlaced to FALSE to exit
        //move current pointer to the next for comparision
      } else {
        current = current->next; 				//advancing to next current
      }
    }
  }
  numberOfTasks++;
  return;
}

/*********************************************************
  function name:        remove
  function inputs:      TCB node pointer
  function outputs:     none
  function description: Remove a task into the taskQueue
                        which is implemented as a circular
                        linked list
  author:               Cuinn Fey
***********************************************************/
void removeTask(TCB* node) {
  if (NULL == head)       // If the head pointer is pointing to nothing
  {
    return;
  }
  else if (head == tail)  // If one node in the list
  {
    if (head == node) {
      head = NULL;
      tail = NULL;
      numberOfTasks--;
    }
  }
  else if (head == node)  // At the head
  {
    head = head -> next;
    head -> prev = tail;            // necessary?
    tail-> next = head;
    numberOfTasks--;
  }
  else if (tail == node) // At the tail
  {
    tail = tail -> prev;
    tail -> next = head;
    head -> prev = tail;
    numberOfTasks--;
  }
  else                   // In the middle
  {
    TCB* current = head;
    while (current->next != tail && current->next != node) {
      current = current->next;
    }
    if (current->next == node) {
      current->next = current->next->next;
      current->next->prev = current;
      numberOfTasks--;
    }
  }
  return;
}

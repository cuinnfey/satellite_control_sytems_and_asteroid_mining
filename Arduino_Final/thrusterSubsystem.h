#ifndef THRUST_SUBSYS_H
#define THRUST_SUBSYS_H


#include "globalVariableInitialization.h"
#include "Arduino_Final.h"
#include "TCBStruct.h"

const int thrusterPin = 22; //the pin to represent out to the thruster 
const int PERIOD_CONST = 500;
const int CYCLE_CONST = 5;

struct thrusterSubsystemDataStruct {
  unsigned short *thrusterCommandPtr;
	unsigned short *fuelLevelPtr;
	float *actFuelPtr;
	unsigned long *thrusterDurationPtr;
	//Bool *thrusterBurn;
};

void thrusterSubsystemFunc (struct thrusterSubsystemDataStruct *dataPtr);

TCB newThrusterSubsystemTask();


#endif

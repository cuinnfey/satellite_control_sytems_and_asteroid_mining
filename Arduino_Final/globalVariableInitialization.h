/*******************************************************
* globalVariableInitialization.h
* Authors: Khang Phan
* Assignment 2
* 10/14/18
* This is header of globalVariableInitialization.c,
* every class include this have access to these global
* variable
*********************************************************/
#ifndef GB_VAR_H
#define GB_VAR_H
#include "TCBStruct.h"

#include <String.h>
#include <WString.h>

#ifndef NULL
#define NULL 0
#endif

#define BATTERY_PIN A11
#define SOLAR_INTERRUPT_PIN 19
#define SOLAR_PWM 22
#define THRUSTER_PWM 24
#define DISTANCE_PIN 23
#define IMAGE_PIN A12
#define BATT_TEMP_PIN1 A14
#define BATT_TEMP_PIN2 A15
#define PHOTON_PIN 34
#define PHASOR_PIN 36

//self destruct
#define DESTRUCT_CODE 5
#define TIME_TO_DESTRUCT 10

#define FLASH1 250
#define FLASH2 500
#define FLASH3 550
#define SECOND 1000
#define TRIGGER_DISTANCE 0

//big-endian (?)
#define CODE_0 31
#define CODE_1 33
#define CODE_2 35
#define CODE_3 37

#define DESTRUCT_PIN 52
#define OVERRIDE_BUTTON 21
#define SIREN 44
#define SIREN_ON 150
#define SIREN_OFF 0

#define PIRATE_DISTANCE_PIN 25
#define NULL_STRING "\0"


#define COMMON_TASK_SIZE 6

enum myBool { FALSE = 0, TRUE = 1 };
typedef enum myBool Bool;

extern unsigned int thrusterCommand;
extern unsigned short batteryLevel;
extern unsigned short fuelLevel;
extern unsigned short powerConsumption;
extern unsigned short powerGeneration;
extern Bool solarPanelState;
extern Bool solarPanelDeploy;
extern Bool solarPanelRetract;
extern Bool fuelLow;
extern Bool batteryLow;
extern double realBattLevel;
extern char command;
extern char response;
extern char commandFromTransport;
extern char responseToTransport;
extern Bool driveMotorSpeedDec;
extern Bool driveMotorSpeedInc;
extern unsigned int solarPWMCounter; 
extern unsigned int solarPWMPulsePoint;
extern unsigned int thrusterPWMCounter; 
extern unsigned int thrusterPWMPulsePoint;
extern unsigned long thrusterDuration;
extern float actFuel;
extern int vReal[256];
extern int vImag[256];

extern Bool batteryOverTemperature;
extern Bool transportDistanceFlag;
extern Bool transportDistanceTaskInQueue;
extern unsigned long prevSuccessTransitionTime;
extern unsigned long successTransitionTime;


extern Bool remoteDisplay;
extern Bool remoteDisplay5SecPassed;
extern Bool imageDataReceived;
extern Bool firePhasor;
extern Bool firePhoton;
extern Bool pirateDetected;
extern Bool pirateDistanceActivated;
extern Bool shootPirate;

extern Bool pirateDistanceFlag;
extern int pirateDistanceMeter;
extern unsigned long pirateSuccessTransitionTime;
extern unsigned long prevPirateSuccessTransitionTime;

//self destructionn timer global variables 
extern unsigned int timeToDestruct;
extern unsigned long destructFlashTimer;
extern unsigned long destructCounterTimer;
extern Bool clearDestructButton;



#define MAX_BATT 36.0
// Buffer for battery level reading
struct BatteryPointerStruct {
  unsigned int battArray[16];
  int front;
};
extern struct BatteryPointerStruct batteryPointer;

// Buffer for transport distance reading
struct TransportDistancePointerStruct {
  int transportDistance[8];
  int front;
};
extern struct TransportDistancePointerStruct transportDistancePointer;

// Buffer for image fft frequency reading
struct ImageBufferStruct {
  int bufferArray[16];
  int front;
};
extern struct ImageBufferStruct imageBuffer;

// Buffer for battery temperature reading
struct BatteryTemperaturePointerStruct {
  unsigned int temperatureMillivolt1[16];
  unsigned int temperatureMillivolt2[16];
  int front;
};
extern struct BatteryTemperaturePointerStruct batteryTemperaturePointer;

extern Bool tempAcknowledge;
extern TCB* commonTaskArray[COMMON_TASK_SIZE];

#define PULSE_PERIOD 500


void initializeGlobalVar();


#endif

/*******************************************************
* taskQueue.h
* Authors: Khang Phan,CuinFey
* Assignment 3
* 10/14/18 updated 11/19/18
* This is header of taskQueue.c, managing the addition
* and removal of tasks
*********************************************************/
#ifndef TASK_QUEUE_H
#define TASK_QUEUE_H

#ifdef __cplusplus
extern "C" {
#endif

#include "globalVariableInitialization.h"
#include "TCBStruct.h"
extern TCB* head;               // Additions
extern TCB* tail;
extern int numberOfTasks;

void insertTask(TCB* task);
void removeTask(TCB* task);

#ifdef __cplusplus
} // extern "C"
#endif
#endif
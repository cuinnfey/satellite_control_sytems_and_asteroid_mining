/*******************************************************
  command.h
  Authors: Khang Phan
  Assignment 5
  11/12/18
  This is header of command.c, containing the data
  struct used by command subsystem
*********************************************************/
#ifndef COMMAND_H
#define COMMAND_H

#include "globalVariableInitialization.h"
#include "TCBStruct.h"
#include "Arduino_Final.h"

struct commandDataStruct {
    Bool *solarPanelStatePtr, *fuelLowPtr, *batteryLowPtr, *batteryOverTemperaturePtr;
    unsigned short *batteryLevelPtr, *fuelLevelPtr, *powerConsumptionPtr, *powerGenerationPtr;
    struct TransportDistancePointerStruct *transportDistancePointerPtr;
    struct BatteryTemperaturePointerStruct *batteryTemperaturePointerPtr;
    struct ImageBufferStruct *imageBufferPtr;
    unsigned int *thrusterCommandPtr;
};
void taskStart();
void taskStop();
void request(String requested , struct commandDataStruct *dataPtr);
void setThruster(String code, unsigned int* thrusterCommandPtr);

TCB newCommandSubsystemTask();
void commandFunc(struct commandDataStruct *dataPtr);
#endif

/*******************************************************
  globalVariableInitialization.c
  Authors: Khang Phan
  Assignment 3
  10/31/18
  This program initialize global variable that others
  can access by importing header.
*********************************************************/
#include "globalVariableInitialization.h"
unsigned int thrusterCommand;
unsigned short batteryLevel;
unsigned short fuelLevel;
unsigned short powerConsumption;
unsigned short powerGeneration;
Bool solarPanelState;
Bool solarPanelDeploy;
Bool solarPanelRetract;
Bool fuelLow;
Bool batteryLow;
double realBattLevel;
char command;
char response;
char commandFromTransport;
char responseToTransport;
Bool driveMotorSpeedDec;
Bool driveMotorSpeedInc;
unsigned int solarPWMCounter;
unsigned int solarPWMPulsePoint;
unsigned long thrusterDuration;
unsigned int thrusterPWMCounter;
unsigned int thrusterPWMPulsePoint;
float actFuel;
int vReal[256];
int vImag[256];
String fromEarth;
String toEarth;

Bool imageDataReceived;

struct BatteryPointerStruct batteryPointer;
struct TransportDistancePointerStruct transportDistancePointer;

Bool imageReadingInterrupted;
struct ImageBufferStruct imageBuffer;
struct BatteryTemperaturePointerStruct batteryTemperaturePointer;

Bool batteryOverTemperature;
Bool transportDistanceFlag;
Bool transportDistanceTaskInQueue;
unsigned long successTransitionTime;
unsigned long prevSuccessTransitionTime;
Bool tempAcknowledge;
Bool remoteDisplay;
Bool remoteDisplay5SecPassed;
Bool shootPirate;

Bool firePhasor;
Bool firePhoton;

Bool pirateDistanceFlag;
Bool pirateDistanceActivated;
unsigned long pirateSuccessTransitionTime;
unsigned long prevPirateSuccessTransitionTime;
Bool pirateDetected;
int pirateDistanceMeter;


//self destructionn timer global variables
unsigned int timeToDestruct;
unsigned long destructFlashTimer;
unsigned long destructCounterTimer;
Bool clearDestructButton;


/************************************************************
  function name:        initializeGlobalVar
  function inputs:      void
  function outputs:     none
  function description: Initialize global variable
  author:               Khang Phan
*************************************************************/
void initializeGlobalVar() {
  thrusterCommand = 0;
  batteryLevel = 100;
  fuelLevel = 100;
  powerConsumption = 0;
  powerGeneration = 0;
  solarPanelState = FALSE;
  solarPanelDeploy = FALSE;
  solarPanelRetract = FALSE;
  fuelLow = FALSE;
  batteryLow = FALSE;
  realBattLevel = MAX_BATT;
  command = NULL;
  response = NULL;
  commandFromTransport = NULL;
  responseToTransport = NULL;
  driveMotorSpeedDec = FALSE;
  driveMotorSpeedInc = FALSE;
  solarPWMCounter = 0;
  solarPWMPulsePoint = 250;
  thrusterPWMCounter = 0;
  thrusterPWMPulsePoint = 0;
  thrusterDuration = 0;
  actFuel = 100;
  fromEarth = NULL_STRING;
  toEarth = NULL_STRING;

  batteryPointer.front = -1;    // flag no data have been read
  transportDistancePointer.front = 0;
  transportDistancePointer.transportDistance[0] = 2000;
  imageBuffer.front = -1;
  batteryTemperaturePointer.front = -1;

  transportDistanceFlag = FALSE;
  transportDistanceTaskInQueue = FALSE;
  batteryOverTemperature = FALSE;
  successTransitionTime = 0;
  prevSuccessTransitionTime = 0;

  tempAcknowledge = FALSE;
  remoteDisplay = FALSE;
  remoteDisplay5SecPassed = FALSE;

  imageDataReceived = FALSE;

  Bool firePhasor;
  firePhoton = FALSE;
  pirateDetected = FALSE;

  pirateDistanceMeter = -1;

  pirateSuccessTransitionTime = 0;
  prevPirateSuccessTransitionTime = 0;
  pirateDistanceActivated = FALSE;
  shootPirate = FALSE;

  timeToDestruct = TIME_TO_DESTRUCT;
  destructFlashTimer = 0;
  destructCounterTimer = 0;
  clearDestructButton = FALSE;
  pirateDistanceFlag = FALSE;
}

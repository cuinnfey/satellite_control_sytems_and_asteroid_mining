/*******************************************************
  scheduler.c
  Authors: Khang Phan
  Assignment 2
  10/14/18
  This program decide which task going to perform next and
  keep the loop if no task is required to be performed
*********************************************************/
#include "scheduler.h"
#include "Arduino_Final.h"

unsigned long lastMajorCycleTime;
unsigned long lastMinorCycleTime;

Bool gunOn = FALSE;

void initializeScheduler();
void reAddTask();

Bool needClear = FALSE;
/*********************************************************
  function name:        doNextTask
  function inputs:      TCB *head, int numTCB, Bool
                        minorCycle
  function outputs:     none
  function description: decide which next task is going
                        to be executed and executes it.
  author:               Khang Phan, Cuinn Fey
  Last modified:        Oct 28th 2018
***********************************************************/
void doNextTask() {
  static int insertIdx = 0;
  static Bool inMinorCycle = TRUE;
  static Bool inMajorCycle = TRUE;
  static Bool pirateDistanceTaskInQueue = FALSE;


  if (head != NULL) {
    head->taskPtr(head->taskDataPtr);
    if (head == &transportDistanceTCB) {
      transportDistanceTaskInQueue = FALSE;
    } else if (head == &pirateDistanceTCB) {
      pirateDistanceTaskInQueue = FALSE;
    } else if (head == &selfDestructTCB) {
      needClear = TRUE;
    } 
    removeTask(head);
  }

  if (millisec - lastMajorCycleTime > 5000) {
    inMajorCycle = TRUE;

    if (remoteDisplay) {
      remoteDisplay5SecPassed = TRUE;
    }

    // Save major cycle start time
    lastMajorCycleTime = millisec;
  }

  if (millisec - lastMinorCycleTime > 5) {
    inMinorCycle = TRUE;

    // Save minor cycle start time
    lastMinorCycleTime = millisec;
  }

  if (inMinorCycle) {
    insertTask(commonTaskArray[insertIdx]);
    if (inMajorCycle) {
      insertIdx = (insertIdx + 1) % COMMON_TASK_SIZE;
      if (insertIdx == 0) {
        inMajorCycle = FALSE;
      }
    }
  }

  if (solarPanelDeploy || solarPanelRetract ) {
    // if deploying or retracting
    static unsigned long lastTimeReadSolarButton = millisec;
    if (millisec - lastTimeReadSolarButton > 2000) {
      // add solar panel control task
      insertTask(&solarControlTCB);

      // draw speed control button
      drawSolarButton1();

      // save last time button read
      lastTimeReadSolarButton = millisec;

      // flag allow input for button
      solarInputMode = 1;
    }
  } else if (solarInputMode != 0) {
    // remove button if they are not needed,
    // revert mode to not accept button press
    solarInputMode = 0;
    drawSolarButton0();
  }

  if (transportDistanceFlag == TRUE && transportDistanceTaskInQueue == FALSE) {
    static unsigned long lastTimeTransportDistanceTask = millisec;
    if (millisec - lastTimeTransportDistanceTask > 50) {
      transportDistanceFlag = FALSE;
      transportDistanceTaskInQueue = TRUE;
      insertTask(&transportDistanceTCB);
      lastTimeTransportDistanceTask = millisec;
    }
  }

  if (pirateDistanceActivated == TRUE && pirateDistanceFlag == TRUE
      && pirateDistanceTaskInQueue == FALSE) {
    static unsigned long lastTimePirateDistanceTask = millisec;
    if (millisec - lastTimePirateDistanceTask > 50) {
      pirateDistanceFlag = FALSE;
      pirateDistanceTaskInQueue = TRUE;
      insertTask(&pirateDistanceTCB);
      lastTimePirateDistanceTask = millisec;
    }
  }

  if (solarPanelState == TRUE) {
    static unsigned long lastReadBattTemp = micros();
    if (500 < micros() - lastReadBattTemp) {
      insertTask(&batteryTemperatureTCB);
      lastReadBattTemp = micros();
    }
  }

  if (imageDataReceived == TRUE) {
    insertTask(&imageCaptureTCB);
    imageDataReceived = FALSE;
  }

  if (EARTH_SERIAL.available() || toEarth != NULL_STRING) {
    insertTask(&satCommSysTCB);
  }

  if (fromEarth != NULL_STRING) {
    insertTask(&commandTCB);
  }

  if (pirateDistanceActivated && pirateDetected) {
    static unsigned long lastTimePirateManagementTask = millisec;
    if (millisec - lastTimePirateManagementTask > 5) {
      insertTask(&pirateManageTCB);
      lastTimePirateManagementTask = millisec;
    }
  } else {
    if (gunOn == TRUE) {
      digitalWrite(PHASOR_PIN, LOW);
      digitalWrite(PHOTON_PIN, LOW);
      gunOn = FALSE;
    }
  }

  static unsigned long lastSelftDestruct = millisec;
  //if the self desctruct needs to be added to queue
  if (pirateDistanceActivated &&
      (TRIGGER_DISTANCE >= pirateDistanceMeter) &&
      ((FALSE == clearDestructButton) || (DESTRUCT_CODE != readDestructCode()))) {
    if ( millisec - lastSelftDestruct > 200) {
      insertTask(&selfDestructTCB);
      lastSelftDestruct = millisec;
    }
  } else {
    //conditional to ensure that the self destruct is set correctly if needed
    // check every 200 millisecs to ensure there is no blocking
    if (millisec - lastSelftDestruct > 200) {
      if (needClear) {
        clearDestruct();
        destructFlashTimer = destructCounterTimer = millisec;
        timeToDestruct = TIME_TO_DESTRUCT;
        analogWrite(SIREN, SIREN_OFF);
        needClear = FALSE;
      }
      clearDestructButton = FALSE;
      lastSelftDestruct = millisec;
    }
  }
  return;
}

/*******************************************************
* consoleDisplay.h
* Authors: Cuinn Fey
* Assignment 2
* 10/14/18
* This is header of consoleDisplay.c, containing the data
* struct used by consoleDisplay
*********************************************************/
#ifndef CONSOL_DISPL_H
#define CONSOL_DISPL_H

#include "globalVariableInitialization.h"
#include "TCBStruct.h"
#include "Arduino.h"
#include "batteryTemperature.h"

extern unsigned long millisec;
extern int consoleMode;

//Structure containing pointers to required variable by consoleDisplayFunc
struct consoleDisplayDataStruct {
    Bool *solarPanelStatePtr, *fuelLowPtr, *batteryLowPtr;
    unsigned short *batteryLevelPtr, *fuelLevelPtr, *powerConsumptionPtr, *powerGenerationPtr;
    struct TransportDistancePointerStruct *transportDistancePointerPtr;
    struct BatteryTemperaturePointerStruct *batteryTemperaturePointerPtr;
    struct ImageBufferStruct *imageBufferPtr;
};

TCB newConsoleDisplayTask();
void consoleDisplayFunc(struct consoleDisplayDataStruct *dataPtr);

#endif

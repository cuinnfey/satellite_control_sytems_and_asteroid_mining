/*******************************************************
  satCommSubsystem.c
  Authors: John McIntyre, Khang Phan
  Assignment 5
  12/08/18
  This program handles satellite communications from and
  to the earth
*********************************************************/

#include "satCommSubsystem.h"

/*********************************************************
  function name:      newSatCommSubsystemTask
  function inputs:    none
  function outputs:   new TCB for satCommSubsystem
  function description: create an satellite comm subsystem TCB with
          its dataPtr initialized.
  author:             John McIntyre
***********************************************************/
TCB newSatCommSubsystemTask() {
  TCB emptyTCB;

  static struct satCommSubsystemDataStruct satCommSubsystemData;

  satCommSubsystemData.fuelLowPtr = &fuelLow;
  satCommSubsystemData.batteryLowPtr = &batteryLow;
  satCommSubsystemData.solarPanelStatePtr = &solarPanelState;
  satCommSubsystemData.batteryLevelPtr = &batteryLevel;
  satCommSubsystemData.fuelLevelPtr = &fuelLevel;
  satCommSubsystemData.powerConsumptionPtr = &powerConsumption;
  satCommSubsystemData.powerGenerationPtr = &powerGeneration;
  satCommSubsystemData.transportDistancePointerPtr = &transportDistancePointer;
  satCommSubsystemData.batteryTemperaturePointerPtr = &batteryTemperaturePointer;
  satCommSubsystemData.imageBufferPtr = &imageBuffer;
  satCommSubsystemData.thrusterCommandPtr = &thrusterCommand;

  emptyTCB.taskPtr = (void*) satCommSubsystemFunc;
  emptyTCB.taskDataPtr = &satCommSubsystemData;
  emptyTCB.priority = 1;
  return emptyTCB;
}

/************************************************************
  function name:      satCommSubsystemFunc
  function inputs:    struct satCommSubsystemDataStruct *dataPtr
  function outputs:   none
  function description: main function of satellite communication
          subsystem TCB, send and receive data to Earth if needed.
          Send display information if needed.
  author:             John McIntyre
*************************************************************/
void satCommSubsystemFunc(struct satCommSubsystemDataStruct *dataPtr)
{
  remoteDisplayUpdate(dataPtr);
  sendData();
  receiveData();

  return;
}

/************************************************************
  function name:      sendData
  function inputs:    none
  function outputs:   none
  function description: Send data to Earth as string with
            ending delimeter be '\0'
  author:             Khang Phan
*************************************************************/
void sendData() {
  if (toEarth != NULL_STRING) {
    EARTH_SERIAL.print(toEarth + '\0');
    toEarth = NULL_STRING;
  }
}

/************************************************************
  function name:      receiveData
  function inputs:    none
  function outputs:   none
  function description: Read data from Earth as string with
            ending delimeter be '\0'
  author:             Khang Phan
*************************************************************/
void receiveData() {
  if (EARTH_SERIAL.available()) {
    fromEarth = EARTH_SERIAL.readStringUntil('\0');
    Serial.println(fromEarth);
  }
}

/************************************************************
  function name:      remoteDisplayUpdate
  function inputs:    struct satCommSubsystemDataStruct *dataPtr
  function outputs:   none
  function description: Send display information to Earth's
              remote display. Ending delimeter is '\0'
  author:             Khang Phan
*************************************************************/
void remoteDisplayUpdate(struct satCommSubsystemDataStruct *dataPtr) {
  if (!(remoteDisplay && remoteDisplay5SecPassed)) {
    return;
  }
  EARTH_SERIAL.print("\n\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n");
  EARTH_SERIAL.print("Intergalactic satellite.\n");
  EARTH_SERIAL.print("Operator: automatic\n");
  EARTH_SERIAL.print("Date: -- no timer --\n");
  EARTH_SERIAL.print("FUEL LOW: ");
  if (*(dataPtr->fuelLowPtr)) {
    EARTH_SERIAL.print("TRUE\n");
  } else {
    EARTH_SERIAL.print("FALSE\n");
  }
  EARTH_SERIAL.print("BATTERY LOW: ");
  if (*(dataPtr->batteryLowPtr)) {
    EARTH_SERIAL.print("TRUE\n");
  } else {
    EARTH_SERIAL.print("FALSE\n");
  }
  if (*(dataPtr->solarPanelStatePtr) == TRUE) {
    EARTH_SERIAL.print("Solar Panels: Deployed\n");
  } else {
    EARTH_SERIAL.print("Solar Panels: Retracted\n");
  }
  EARTH_SERIAL.print("Battery Level: ");
  EARTH_SERIAL.print(*(dataPtr->batteryLevelPtr));
  EARTH_SERIAL.print("% - ");
  EARTH_SERIAL.print(realBattLevel, 1);
  EARTH_SERIAL.print(" V\n");
  EARTH_SERIAL.print("Fuel Level: ");
  EARTH_SERIAL.print(*(dataPtr->fuelLevelPtr));
  EARTH_SERIAL.print('\n');
  EARTH_SERIAL.print("Power consumption: ");
  EARTH_SERIAL.print(*(dataPtr->powerConsumptionPtr));
  EARTH_SERIAL.print('\n');
  EARTH_SERIAL.print("Power generation: ");
  EARTH_SERIAL.print(*(dataPtr->powerGenerationPtr));
  EARTH_SERIAL.print('\n');
  EARTH_SERIAL.print("Mining vehicle distance: ");
  EARTH_SERIAL.print(dataPtr->transportDistancePointerPtr->transportDistance[dataPtr->transportDistancePointerPtr->front]);
  EARTH_SERIAL.print(" meters\n");
  EARTH_SERIAL.print("Temperature 1: ");
  EARTH_SERIAL.print(millivoltToCelcius(dataPtr->batteryTemperaturePointerPtr->temperatureMillivolt1[dataPtr->batteryTemperaturePointerPtr->front]));
  EARTH_SERIAL.print(" C\n");
  EARTH_SERIAL.print("Temperature 2: ");
  EARTH_SERIAL.print(millivoltToCelcius(dataPtr->batteryTemperaturePointerPtr->temperatureMillivolt2[dataPtr->batteryTemperaturePointerPtr->front]));
  EARTH_SERIAL.print(" C\n");
  EARTH_SERIAL.print("Image peak frequency: ");
  EARTH_SERIAL.print(dataPtr->imageBufferPtr->bufferArray[imageBuffer.front]);
  EARTH_SERIAL.print(" Hz\n");
  EARTH_SERIAL.print('\0');
  remoteDisplay5SecPassed = FALSE;
}

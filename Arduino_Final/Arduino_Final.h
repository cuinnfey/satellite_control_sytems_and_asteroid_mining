/*******************************************************
  Arduino_Final.h
  Authors: Khang Phan
  Assignment 2
  10/14/18
  This is header of Arduino_final.ino, importing all the
  require task classes and scheduler, as well as required
  library, definitions to use with tft display and touch
  screen
*********************************************************/
#ifndef ARDUINO_FINAL_H
#define ARDUINO_FINAL_H

// Tasks and schedulers
#include "globalVariableInitialization.h"
#include "TCBStruct.h"
#include "powerSubsystem.h"
#include "warningAlarm.h"
#include "scheduler.h"
#include "consoleDisplay.h"
#include "thrusterSubsystem.h"
#include "thrusterSubsystem.h"
#include "satCommSubsystem.h"
#include "vehicleComms.h"
#include "solarPanelControl.h"
#include "transportDistance.h"
#include "imageCapture.h"
#include "batteryTemperature.h"
#include "command.h"
#include "pirateDistance.h"
#include "pirateManagement.h"
#include "destructSubsystem.h"

// Touch screen and tft library
#include <Elegoo_GFX.h>    // Core graphics library
#include <Elegoo_TFTLCD.h> // Hardware-specific library
#include <stdint.h>
#include "TouchScreen.h"

// Pins used by touch screen
#define YP A2  // must be an analog pin, use "An" notation!
#define XM A3  // must be an analog pin, use "An" notation!
#define YM 8   // can be a digital pin
#define XP 9   // can be a digital pin

// The control pins for the LCD can be assigned to any digital or
// analog pins...but we'll use the analog pins as this allows us to
// double up the pins with the touch screen (see the TFT paint example).
#define LCD_CS A3 // Chip Select goes to Analog 3
#define LCD_CD A2 // Command/Data goes to Analog 2
#define LCD_WR A1 // LCD Write goes to Analog 1
#define LCD_RD A0 // LCD Read goes to Analog 0

#define LCD_RESET A4 // Can alternately just connect to Arduino's reset pin

// When using the BREAKOUT BOARD only, use these 8 data lines to the LCD:
// For the Arduino Uno, Duemilanove, Diecimila, etc.:
//   D0 connects to digital pin 8  (Notice these are
//   D1 connects to digital pin 9   NOT in order!)
//   D2 connects to digital pin 2
//   D3 connects to digital pin 3
//   D4 connects to digital pin 4
//   D5 connects to digital pin 5
//   D6 connects to digital pin 6
//   D7 connects to digital pin 7
// For the Arduino Mega, use digital pins 22 through 29
// (on the 2-row header at the end of the board).

// Assign human-readable names to some common 16-bit color values:
#define  BLACK   0x0000
#define BLUE    0x001F
#define RED     0xF000
#define GREEN   0x07E0
#define CYAN    0x07FF
#define MAGENTA 0xF81F
#define YELLOW  0xFFE0
#define WHITE   0xFFFF
#define ORANGE  0xFA60
#define USE_Elegoo_SHIELD_PINOUT

#define VEHICLE_SERIAL Serial2
#define EARTH_SERIAL Serial3

#define TEMP_X_MIN 120
#define TEMP_X_MAX 570
#define TEMP_Y_MIN 380
#define TEMP_Y_MAX 620



extern TCB powerSubsysTCB;             // Power Subsystem TCB
extern TCB warnAlarTCB;                // Warning Alarm Subsystem TCB
extern TCB consoleDispTCB;             // Console Display Subsystem TCB
extern TCB thrusterSubsysTCB;          // Thruster Subsystem TCB
extern TCB satCommSysTCB;              // Satellite Communications Subsystem TCB
extern TCB vehicleCommsTCB;            // Mining Vehicle Communications Subsystem TCB
extern TCB solarControlTCB;
extern TCB transportDistanceTCB;
extern TCB imageCaptureTCB;
extern TCB batteryTemperatureTCB;
extern TCB commandTCB;
extern TCB pirateDistanceTCB;
extern TCB pirateManageTCB;
extern TCB selfDestructTCB;

extern String fromEarth;
extern String toEarth;


void drawSolarButton1();
void drawSolarButton0();
#endif

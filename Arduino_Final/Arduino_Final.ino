/*******************************************************
  Arduino_Final.ino
  Authors: Khang Phan
  Assignment 2
  10/14/18
  The main execute program that run on Arduino MEGA 2560,
  represent satellite management system
*********************************************************/
#include "Arduino_Final.h"

Elegoo_TFTLCD tft(LCD_CS, LCD_CD, LCD_WR, LCD_RD, LCD_RESET);
// If using the shield, all control and data lines are fixed, and
// a simpler declaration can optionally be used:
// Elegoo_TFTLCD tft;

TouchScreen ts = TouchScreen(XP, YP, XM, YM, 300);
// For better pressure precision, we need to know the resistance
// between X+ and X- Use any multimeter to read it
// For the one we're using, its 300 ohms across the X plate

const int t3_load = 0;          // initial Timer3 count value

// Arduino Mega 2560 Timer3 use 16Mhz clock, which require 16000
// cycle to reach 1 milli second interval
const int t3_comp = 16000;

unsigned long millisec = 0;     // store real time millisecond value

int consoleMode = 0;            // Console display mode, 0 for status
// and 1 for anunciation
int solarInputMode = 0;         // Console display mode, 1 to allow input
// 2 to lock and 0 to not display

Bool measureBatteryInterrupt = FALSE;

TCB* commonTaskArray[COMMON_TASK_SIZE];              // declare task queue

TCB powerSubsysTCB;             // Power Subsystem TCB
TCB warnAlarTCB;                // Warning Alarm Subsystem TCB
TCB consoleDispTCB;             // Console Display Subsystem TCB
TCB thrusterSubsysTCB;          // Thruster Subsystem TCB
TCB satCommSysTCB;              // Satellite Communications Subsystem TCB
TCB vehicleCommsTCB;            // Mining Vehicle Communications Subsystem TCB
TCB solarControlTCB;
TCB transportDistanceTCB;
TCB imageCaptureTCB;
TCB batteryTemperatureTCB;
TCB commandTCB;
TCB pirateDistanceTCB;
TCB pirateManageTCB;
TCB selfDestructTCB;

// Declare functions will be called
void timerSetup ();
void TFTsetup ();

/*********************************************************
  function name:        setup
  function inputs:      none
  function outputs:     none
  function description: preliminary set up before main loop
                body
  author:               Khang Phan
***********************************************************/
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  VEHICLE_SERIAL.begin(4800);
  EARTH_SERIAL.begin(4800);
  timerSetup();
  TFTsetup ();

  powerSubsysTCB = newPowerSubsystemTask();
  warnAlarTCB = newWarningAlarmTask();
  consoleDispTCB = newConsoleDisplayTask();
  thrusterSubsysTCB = newThrusterSubsystemTask();
  satCommSysTCB = newSatCommSubsystemTask();
  vehicleCommsTCB = newVehicleCommsSubsystemTask();
  solarControlTCB = newSolarPanelControlTask();
  transportDistanceTCB = newTransportDistanceTask();
  imageCaptureTCB = newImageCaptureSubsystemTask();
  batteryTemperatureTCB = newBatteryTempTask();
  commandTCB = newCommandSubsystemTask();
  pirateDistanceTCB = newPirateDistanceTask();
  pirateManageTCB = newpirateManagementTask();
  selfDestructTCB = newDestructSubsystemTask();

  // Array of task to be reference and iterate over when
  // re-adding task every major cycle
  commonTaskArray[0] = &warnAlarTCB;
  commonTaskArray[1] = &powerSubsysTCB;
  commonTaskArray[2] = &satCommSysTCB;
  commonTaskArray[3] = &thrusterSubsysTCB;
  commonTaskArray[4] = &vehicleCommsTCB;
  commonTaskArray[5] = &consoleDispTCB;

  initializeGlobalVar();
  //initializeScheduler();
  pinMode(SOLAR_INTERRUPT_PIN, INPUT);

  // set up interrupt button saying that solar panel is fully deployed or
  // retracted
  attachInterrupt( digitalPinToInterrupt(SOLAR_INTERRUPT_PIN) , solarInterrupt, RISING);

  pinMode(DISTANCE_PIN, INPUT);
  pinMode(PHOTON_PIN, OUTPUT);
  pinMode (PHASOR_PIN, OUTPUT);
  pinMode(DESTRUCT_PIN, OUTPUT);

  //set up for reading the self destruct override code
  pinMode(CODE_0, INPUT);
  pinMode(CODE_1, INPUT);
  pinMode(CODE_2, INPUT);
  pinMode(CODE_3, INPUT);

  //setup of destruction override button
  pinMode(OVERRIDE_BUTTON, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(OVERRIDE_BUTTON), destructButtonInterrupt, FALLING);
}


/*********************************************************
  function name:        loop
  function inputs:      none
  function outputs:     none
  function description: Main loop body of the arduino
  author:               Khang Phan
***********************************************************/
void loop() {
  //Serial.println(thrusterDuration);
  static Bool wasTouch = FALSE;     // Was it touch screen being touch
  // since last loop
  // a point object holds x y and z coordinates when touched
  TSPoint p = ts.getPoint();
  pinMode(YP, OUTPUT);            //.kbv these pins are shared with TFT
  pinMode(XM, OUTPUT);            //.kbv these pins are shared with TFT
  if (!wasTouch) {
    if (p.z > ts.pressureThreshhold) {
      wasTouch = TRUE;
      if (p.x < 884 && p.x > 696 && p.y < 420 && p.y > 94) {
        // update console display mode
        consoleMode = (consoleMode + 1) % 2;
        // redraw button
        drawStatusModeButton();
      } else if (solarInputMode == 1 && p.x < 940 && p.x > 610 && p.y < 610 && p.y > 380) {
        // flag solar panel motor drive speed increase by touch button
        driveMotorSpeedInc = TRUE;
        drawSolarButton2();
        solarInputMode = 2;
      } else if (solarInputMode == 1 && p.x < 940 && p.x > 610 && p.y < 860 && p.y > 620) {
        // flag solar panel motor drive speed decrease by touch button
        driveMotorSpeedDec = TRUE;
        drawSolarButton2();
        solarInputMode = 2;
      }
      if (batteryOverTemperature && p.x > TEMP_X_MIN && p.x < TEMP_X_MAX && p.y > TEMP_Y_MIN && p.y < TEMP_Y_MAX) {
        //Flaf the acknowledgement of the tempurature and reset of the over tempurature flag
        tempAcknowledge = TRUE;
        batteryOverTemperature = FALSE;
      }
    }
  } else {
    if (p.x == 0 && p.z == 0) {
      // user released touch
      wasTouch = FALSE;
    }
  }

  // Read command from user input
  if (Serial.available() > 0) {
    String inputLine = Serial.readStringUntil('\r');
    inputLine.toUpperCase();
    if (inputLine == "F" || inputLine == "B"
        || inputLine == "L" || inputLine == "R"
        || inputLine == "D" || inputLine == "H"
        || inputLine == "S" || inputLine == "I" ) {
      command = inputLine.charAt(0);
      Serial.print("Sending command to mining vehicle in next cycle: ");
      Serial.println(command);
    } else if (inputLine == "DEPLOY") {
      solarPanelRetract = TRUE;
      solarPanelDeploy = FALSE;
      solarPWMCounter = 0;
    } else if (inputLine == "RETRACT") {
      solarPanelRetract = FALSE;
      solarPanelDeploy = TRUE;
      solarPWMCounter = 0;
    } else if (inputLine == "FULL") {
      solarInterrupt();
    } else if (inputLine == "" && pirateDistanceActivated == TRUE) {
      shootPirate = TRUE;
    } else {
      Serial.println("Invalid command to mining vehicle.");
    }
  }

  // Read response from mining/transport vehicle
  if (VEHICLE_SERIAL.available()) {
    String receivedString = VEHICLE_SERIAL.readStringUntil('\0');
    receivedString.toUpperCase();
    char received = receivedString.charAt(0);
    if (received == 'A') {
      Serial.print("Recieved afirmative from mining vehicle: ");
      Serial.println(receivedString);
      command = NULL;
      response = NULL;
    } else if (receivedString == "T") {
      Serial.println("Receive request to lift off from transport vehicle: T. Sending OK to lift off next cycle.");
      commandFromTransport = 'T';
    } else if (receivedString == "D") {
      Serial.println("Receive request to dock from transport vehicle: D. Sending confirm dock next cycle.");
      commandFromTransport = 'D';
    } else if (receivedString == "W") {
      Serial.println("Image capture complete: W");
      command = NULL;
    } else if (receivedString == "P") {
      char temp[256];
      // Write a dummy character signal we
      // are ready to receive 256 byte of 
      // information
      VEHICLE_SERIAL.write('\0');
      VEHICLE_SERIAL.readBytes(temp, 256);
      for (int i = 0; i < 256; i++) {
        vReal[i] = temp[i];
      }
      Serial.println("Received image data: P");
      imageDataReceived = TRUE;
      command = NULL;
    }
  }


  static Bool prevDistanceState = FALSE;
  // Timing of transition from high to low of transport distance signal
  if (prevDistanceState) {
    if (digitalRead(DISTANCE_PIN) == LOW) {
      prevSuccessTransitionTime = successTransitionTime;
      successTransitionTime = micros();
      transportDistanceFlag = TRUE;
      prevDistanceState = FALSE;
    }
  } else {
    if (digitalRead(DISTANCE_PIN) == HIGH) {
      prevDistanceState = TRUE;
    }
  }


  static Bool prevPirateDistanceState = FALSE;
  // Timing of transition from high to low of pirate distance signal
  if (prevPirateDistanceState) {
    if (digitalRead(PIRATE_DISTANCE_PIN) == LOW) {
      prevPirateSuccessTransitionTime = pirateSuccessTransitionTime;
      pirateSuccessTransitionTime = micros();
      //Serial.println(1000000L/(pirateSuccessTransitionTime-prevPirateSuccessTransitionTime));
      pirateDistanceFlag = TRUE;
      prevPirateDistanceState = FALSE;
    }
  } else {
    if (digitalRead(PIRATE_DISTANCE_PIN) == HIGH) {
      prevPirateDistanceState = TRUE;
    }
  }

  // put your main code here, to run repeatedly:
  doNextTask();
  //Serial.println(getDistanceMeter());
}

/*********************************************************
  function name:        timerSetup
  function inputs:      none
  function outputs:     none
  function description: Set up 1us counter using Arduino
                MEGA Timer3 and internal 16Mhz clock
  author:               Khang Phan
***********************************************************/
void timerSetup () {
  // reset Timer3 control Reg A
  TCCR3A = 0;

  // set prescaler of Timer3 to 1
  TCCR3B &= ~(1 << CS12);
  TCCR3B &= ~(1 << CS11);
  TCCR3B |= (1 << CS10);

  // reset count of timer 3 and set compare value
  TCNT3 = t3_load;
  OCR3A = t3_comp;

  // enable Timer3 compare interrupt
  TIMSK3 = (1 << OCIE3A);

  // enable global interrupt
  sei();
}

// flag current output of solar panel motor and thruster
// PWM pin
Bool solarPinHigh = FALSE;
Bool thrusterPinHigh = FALSE;
/*********************************************************
  function name:        ISR
  function inputs:      none
  function outputs:     none
  function description: Set up interuption at 1ms, update
                system timer millisec, used to create
                PWM signal and interrupt to read battery
  author:               Khang Phan
***********************************************************/
ISR(TIMER3_COMPA_vect) {
  TCNT3 = t3_load;
  millisec++;

  // read battery while interrupt
  if (measureBatteryInterrupt) {
    readBatteryInterruptFunction();
  }

  // create solar motor drive PWM signal
  if (solarPanelDeploy || solarPanelRetract) {
    solarPWMCounter = (solarPWMCounter + 1) % PULSE_PERIOD;
    if (solarPinHigh && solarPWMCounter > solarPWMPulsePoint) {
      // PWM signal go low when counter is over pulse point
      solarPinHigh = FALSE;
      digitalWrite(SOLAR_PWM, LOW);
    } else if (!solarPinHigh && solarPWMCounter < solarPWMPulsePoint) {
      // PWM signal go high when counter is under pulse point
      digitalWrite(SOLAR_PWM, HIGH);
      solarPinHigh = TRUE;
    }
  }

  // thruster drive PWM signal
  if (thrusterDuration > 0) {
    // keep sending signal until duration go to 0
    thrusterDuration--;
    thrusterPWMCounter = (thrusterPWMCounter + 1) % PULSE_PERIOD;
    if (thrusterPinHigh && thrusterPWMCounter > thrusterPWMPulsePoint) {
      // PWM signal go low when counter is over pulse point
      thrusterPinHigh = FALSE;
      digitalWrite(THRUSTER_PWM, LOW);
    } else if (!thrusterPinHigh && thrusterPWMCounter < thrusterPWMPulsePoint) {
      // PWM signal go high when counter is under pulse point
      digitalWrite(THRUSTER_PWM, HIGH);
      thrusterPinHigh = TRUE;
    }
  } else {
    //turn off pwm signal when duration is 0
    if (thrusterPinHigh && thrusterPWMCounter > thrusterPWMPulsePoint) {
      thrusterPinHigh = FALSE;
      digitalWrite(THRUSTER_PWM, LOW);
    }
  }
}

/*********************************************************
  function name:        TFTsetup
  function inputs:      none
  function outputs:     none
  function description: Set up interuption at 1ms, update
                system timer millisec
  author:               Editted from TFT example on class
                page
***********************************************************/
void TFTsetup () {
  Serial.println(F("Connecting to TFT annunciation board..."));

  tft.reset();

  uint16_t identifier = tft.readID();
  if (identifier == 0x9325) {
    Serial.println(F("Found ILI9325 LCD driver"));
  } else if (identifier == 0x9328) {
    Serial.println(F("Found ILI9328 LCD driver"));
  } else if (identifier == 0x4535) {
    Serial.println(F("Found LGDP4535 LCD driver"));
  } else if (identifier == 0x7575) {
    Serial.println(F("Found HX8347G LCD driver"));
  } else if (identifier == 0x9341) {
    Serial.println(F("Found ILI9341 LCD driver"));
  } else if (identifier == 0x8357) {
    Serial.println(F("Found HX8357D LCD driver"));
  } else if (identifier == 0x0101)
  {
    identifier = 0x9341;
    Serial.println(F("Found 0x9341 LCD driver"));
  }
  else if (identifier == 0x1111)
  {
    identifier = 0x9328;
    Serial.println(F("Found 0x9328 LCD driver"));
  }
  else {
    Serial.print(F("Unknown LCD driver chip: "));
    Serial.println(identifier, HEX);
    Serial.println(F("If using the Elegoo 2.8\" TFT Arduino shield, the line:"));
    Serial.println(F("  #define USE_Elegoo_SHIELD_PINOUT"));
    Serial.println(F("should appear in the library header (Elegoo_TFT.h)."));
    Serial.println(F("If using the breakout board, it should NOT be #defined!"));
    Serial.println(F("Also if using the breakout, double-check that all wiring"));
    Serial.println(F("matches the tutorial."));
    identifier = 0x9328;

  }

  Serial.println(F("Welcome to Satellite console panel!"));
  tft.begin(identifier);

  tft.fillScreen(BLACK);
  // Rotate screen to landscape mode
  tft.setRotation(1);
  // Draw initial button that change console display mode
  drawStatusModeButton();
}

/*********************************************************
  function name:        drawStatusModeButton 
  function inputs:      none
  function outputs:     none
  function description: Draw button on TFT screen depend
                on the console display mode
  author:               Khang Phan, Cuinn Fey, John McIntyre
***********************************************************/
void drawStatusModeButton() {
  if (consoleMode == 0) {
    // Draw status mode
    tft.fillRoundRect(185, 2, 133, 70, 5, BLUE);
    tft.setTextColor(WHITE);
    tft.setTextSize(3);
    tft.setCursor(200, 12);
    tft.print("STATUS");
    tft.setCursor(220, 40);
    tft.print("MODE");
  }

  if (consoleMode == 1) {
    // Draw annunciation mode
    tft.fillRoundRect(185, 2, 133, 70, 5, CYAN);
    tft.setTextColor(RED);
    tft.setTextSize(3);
    tft.setCursor(205, 12);
    tft.print("ANUNC.");
    tft.setCursor(220, 40);
    tft.print("MODE");
  }
}

/*********************************************************
  function name:        drawSolarButton1 
  function inputs:      none
  function outputs:     none
  function description: Draw button on TFT screen 
          when we allow solar panel control input
  author:               Khang Phan, Cuinn Fey, John McIntyre
***********************************************************/
void drawSolarButton1() {
  tft.fillRoundRect(185, 77, 133, 70, 5, GREEN);
  tft.setTextColor(ORANGE);
  tft.setTextSize(3);
  tft.setCursor(207, 87);
  tft.print("+SOL");
  tft.setCursor(250, 113);
  tft.print("5%");

  tft.fillRoundRect(185, 150, 133, 70, 5, GREEN);
  tft.setTextColor(ORANGE);
  tft.setTextSize(3);
  tft.setCursor(205, 160);
  tft.print("-SOL");
  tft.setCursor(250, 193);
  tft.print("5%");
}

/*********************************************************
  function name:        drawSolarButton2 
  function inputs:      none
  function outputs:     none
  function description: Draw button on TFT screen 
            when we already read solar panel control input
  author:               Khang Phan, Cuinn Fey, John McIntyre
***********************************************************/
void drawSolarButton2() {
  tft.fillRoundRect(185, 77, 133, 70, 5, ORANGE);
  tft.setTextColor(CYAN);
  tft.setTextSize(3);
  tft.setCursor(205, 87);
  tft.print("+SOL");
  tft.setCursor(250, 113);
  tft.print("5%");

  tft.fillRoundRect(185, 150, 133, 70, 5, ORANGE);
  tft.setTextColor(CYAN);
  tft.setTextSize(3);
  tft.setCursor(205, 160);
  tft.print("-SOL");
  tft.setCursor(250, 193);
  tft.print("5%");
}

/*********************************************************
  function name:        drawSolarButton0
  function inputs:      none
  function outputs:     none
  function description: remove the button that drawed by
                drawSolarButton1() or drawSolarButton2()
  author:               Khang Phan and Cuinn Fey
***********************************************************/
void drawSolarButton0() {
  tft.fillRect(185, 77, 266, 180, BLACK);
}

/*********************************************************
  function name:        solarInterrupt
  function inputs:      none
  function outputs:     none
  function description: interrupt function when solar panel
            is fully deployed or retracted
  author:               Khang Phan
***********************************************************/
void solarInterrupt()
{
  if (!solarPanelDeploy && !solarPanelRetract) {
    // Do nothing if the solar panel is not trying to
    // deploy or retract
    return;
  }

  if (solarPanelRetract) {
    // Update state and stop retracting when fully retracted
    solarPanelState = FALSE;
    solarPanelRetract = FALSE;
  }

  if (solarPanelDeploy) {
    // Update state and stop deploying when fully deployed
    solarPanelState = TRUE;
    solarPanelDeploy = FALSE;
  }

  solarPinHigh = FALSE;
  digitalWrite(SOLAR_PWM, LOW);
}


/*********************************************************
  function name:        destructButtonInterrupt
  function inputs:      none
  function outputs:     none
  function description: interrupt function when acting as
            the second factor to disarm the
            self destruct procedure
  author:               John McIntyre
***********************************************************/
void destructButtonInterrupt() {
  //setting the button to deactive destruction to True - pressed
  clearDestructButton = TRUE;
}


/************************************************************
  function name:        readCode
  function inputs:      void
  function outputs:     unsigned integer of the code enter to halt desctruct
  function description: Function to read the code enter to stop
            desctruction countdown -
            processed as a big-endian number
  author:               John McIntyre
*************************************************************/
int readDestructCode() {

  // initial setting of rtn variable
  int code = 0;

  //settting value if switch 1 is closed
  if (HIGH  == digitalRead(CODE_0)) {
    code += 8;
  } else {
    code += 0;
  }

  //settting value if switch 2 is closed
  if (HIGH == digitalRead(CODE_1)) {
    code += 4;
  } else {
    code += 0;
  }

  //settting value if switch 3 is closed
  if (HIGH == digitalRead(CODE_2)) {
    code += 2;
  } else {
    code += 0;
  }

  //settting value if switch 4 is closed
  if (HIGH == digitalRead(CODE_3)) {
    code += 1;
  } else {
    code += 0;
  }

  return code;
}

/************************************************************
  function name:        clearDestruct
  function inputs:      void
  function outputs:     none
  function description: Function to clear the self destruct
            sequnce after it as started
  author:               John McIntyre
*************************************************************/
void clearDestruct() {
  //black rectangles to remove destruction display
  tft.fillRect(0, 112, 170, 55, BLACK);
  tft.fillRect(0, 170, 80, 45, BLACK);
}

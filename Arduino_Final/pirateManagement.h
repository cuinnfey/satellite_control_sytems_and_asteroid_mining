/*******************************************************
  pirateManagement.h
  Authors: Cuinn Fey
  Assignment 5
  12/7/18
  This is header of pirateManagement.c, containing the data
  struct used by transportManagement
*********************************************************/
#ifndef	PIRATE_MANAGEMENT_H
#define PIRATE_MANAGEMENT_H


#include <Arduino.h>
//#ifdef __cplusplus
//extern "C" {
//#endif

#include "globalVariableInitialization.h"
#include "TCBStruct.h"

extern Bool gunOn;

struct pirateManagementDataStruct {
  int *pirateDistanceMeterPtr;
};

TCB newpirateManagementTask();
void pirateManagementFunc (struct pirateManagementDataStruct *dataPtr);
//#ifdef __cplusplus
//} // extern "C"
//#endif
#endif

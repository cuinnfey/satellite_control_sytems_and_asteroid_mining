#include "thrusterSubsystem.h"

TCB newThrusterSubsystemTask() {
    TCB emptyTCB;

    static struct thrusterSubsystemDataStruct thrusterSubsystemData;
	
    
	thrusterSubsystemData.fuelLevelPtr = &fuelLevel;
	thrusterSubsystemData.thrusterCommandPtr = &thrusterCommand;
	thrusterSubsystemData.actFuelPtr = &actFuel;
	thrusterSubsystemData.thrusterDurationPtr = &thrusterDuration;
	//thrusterSubsystemData.thrusterBurnPtr = &thrusterBurn;

    emptyTCB.taskPtr = (void*) thrusterSubsystemFunc;
    emptyTCB.taskDataPtr = &thrusterSubsystemData;
    emptyTCB.priority = 3;
    return emptyTCB;
}

void thrusterSubsystemFunc(struct thrusterSubsystemDataStruct *dataPtr)
{

	if (0 < *(dataPtr->thrusterDurationPtr)){
		return;
	}
	
	//float that represent the use of fuel of firing the
    //thruster at 50% for one second
    float useUnit = .00000643;

    //coeffiecent that scale fuel usage based on thruster
    float powerCo = 0;

    //set up of variable from the command
    int left = 0;
    int right = 0;
    int up = 0;
    int down = 0;

    int power = 0;
    int dur = 0;

    //setting the representation to thruster control 0000, 0001, 0011, 0111, 1111
    unsigned int pwr_00 = 0x0;            //0% as 0000
    unsigned int pwr_25 = 0x1;           //25% as 0001
    unsigned int pwr_50 = 0x3;           //50% as 0011
    unsigned int pwr_75 = 0x7;           //75% as 0111
    unsigned int pwr_100 = 0xf;          //100% as 1111


    //masks for getting information out
    unsigned int durMask = 0xFF00;       //to isolate bits 15 to 8
    unsigned int powerMask = 0xF0;       //to isolate bits 7 to 4
    unsigned int thrusterMask = 0xF;     //to isolate the 3 to 0

    //values unscale word in command
    unsigned int dur_offset = 0x100;     //to place the duration values in bits 15 - 8
    unsigned int pwr_offset = 0x10;      //to place the power values in bits 7 - 4

    //capture of duration value
    dur = (*(dataPtr->thrusterCommandPtr) & durMask) / dur_offset;

    //capture of the power value in command string
    int temp = (*(dataPtr->thrusterCommandPtr) & powerMask) / pwr_offset;

    if (pwr_00 == temp){             //0% power
        power = 0;
        powerCo = 0;
    }else if (pwr_25 == temp){        //25% power
        power = 25;
        powerCo = 0.50;
    }else if(pwr_50 == temp){        //50% power
        power = 50;
        powerCo = 1.0;
    }else if(pwr_75 == temp){        //75% power
        power = 75;
        powerCo = 1.50;
    }else{                          //100% power
        power = 100;
        powerCo = 2.0;
    }

    //assign thruster
    int t_indx = (*(dataPtr->thrusterCommandPtr) & thrusterMask);
    if (0 == t_indx){               //thruster 0 activated - left
        left = 1;
    } else if ( 1 == t_indx){       //thruster 1 activate - right
        right = 1;
    } else if (2 == t_indx){        //thruster 2 activate - up
        up = 1;
    } else if (3 == t_indx){        //thruster 3 activate - dowm
        down = 1;
    }

	//reset of thruster command to reset to 0
	*(dataPtr->thrusterCommandPtr) = 0;
	//thrusterProcess = TRUE;
	
    //computation of fuel consumption from command
  *(dataPtr->actFuelPtr) = *(dataPtr->actFuelPtr) - (dur * powerCo * useUnit);
	*(dataPtr->fuelLevelPtr) = (unsigned short int) *(dataPtr->actFuelPtr);
	
	*(dataPtr->thrusterDurationPtr) = ((unsigned long)dur) * 1000;
	thrusterPWMPulsePoint = PULSE_PERIOD * power / 100;	
}

//void thrusterPWM(int power, unsigned int dur){
//	//int pwmScale = 255;
//	thrusterDuration = (unsigned long)dur * 1000;
//	//*(dataPtr->thrusterBurnPtr) = TRUE;
//	analogWrite(THRUSTER_PWM, power);
//  //analogWrite(THRUSTER_PWM, 100);
//	
//	
//}

/*******************************************************
  pirateManagement.c
  Authors: Cuinn Fey
  Assignment 5
  12/5/18

*********************************************************/
#include "pirateManagement.h"

/*********************************************************
  function name:        newpirateManagementTask
  function inputs:      none
  function outputs:     new TCB for pirateManagement
  function description: create an pirate management TCB with
                         its dataPtr initialized.
  author:               Cuinn Fey
***********************************************************/
TCB newpirateManagementTask() {
  TCB emptyTCB;
  static struct pirateManagementDataStruct pirateManagementData;

  pirateManagementData.pirateDistanceMeterPtr = &pirateDistanceMeter;
  emptyTCB.taskPtr = (void*) pirateManagementFunc;
  emptyTCB.taskDataPtr = &pirateManagementData;
  emptyTCB.priority = 5;
  return emptyTCB;
}

/************************************************************
  function name:        pirateManagementFunc
  function inputs:      struct pirateManagementDataStruct *dataPtr
  function outputs:     none
  function description: main function of pirateManagement
  author:               Cuinn Fey
*************************************************************/
void pirateManagementFunc (struct pirateManagementDataStruct *dataPtr) {
  //  Serial.println(*(dataPtr->pirateDistanceMeterPtr));
  //  Serial.println(pirateDistanceMeter);
  //  Serial.println((long)&pirateDistanceMeter);
  static unsigned long lastShoot = millis();
  if (shootPirate == TRUE) {
    if (*(dataPtr->pirateDistanceMeterPtr) <= 5) {
      // print Photon fire
      if (gunOn == FALSE) {
        digitalWrite(PHOTON_PIN, HIGH);
        gunOn = TRUE;
      }
      lastShoot = millis();
    } else if (*(dataPtr->pirateDistanceMeterPtr) <= 30) {
      // print Phasor fire
      if (gunOn == FALSE) {
        digitalWrite(PHASOR_PIN, HIGH);
        gunOn = TRUE;
      }
      lastShoot = millis();
    } else {
      Serial.println("There are no pirate, don't waste energy!");
    }
    shootPirate = FALSE;
  } else if (millis() - lastShoot > 100 && gunOn == TRUE) {
    digitalWrite(PHASOR_PIN, LOW);
    digitalWrite(PHOTON_PIN, LOW);
    gunOn = FALSE;
  }
  return;
}

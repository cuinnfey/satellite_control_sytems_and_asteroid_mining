/*******************************************************
* warningAlarm.h
* Authors: Khang Phan
* Assignment 2
* 10/14/18
* This is header of warningAlarm.c, connecting to the
* Arduino's TFT as well as the global counter/timer
* millisec
*********************************************************/
#ifndef WARNING_ALARM_H
#define WARNING_ALARM_H

#include "Arduino_Final.h"
#include <Elegoo_TFTLCD.h>
#include "globalVariableInitialization.h"
#include "TCBStruct.h"

struct warningAlarmDataStruct {
    Bool *fuelLowPtr, *batteryLowPtr, *batteryOverTemperaturePtr;
    unsigned short *batteryLevelPtr, *fuelLevelPtr;
};

// tft display
extern Elegoo_TFTLCD tft;

// global timer/counter
extern unsigned long millisec;

//acknowledgement variable 
extern Bool tempAcknowledge;

void  warningAlarmFunc(struct warningAlarmDataStruct *dataPtr);
void printBattery(int state);
void printFuel(int state);
void printTemp(int state);
TCB newWarningAlarmTask();

#endif

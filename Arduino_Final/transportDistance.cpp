/*******************************************************
  transportDistance.c
  Authors: Khang Phan
  Assignment 4
  11/15/18
  Measuring the distance between transport vehicle and
  satellite using a signal.
*********************************************************/
#include "transportDistance.h"

#define MAX_FREQ 80L
#define MIN_FREQ 4L
#define MAX_DIST 2000L
#define MIN_DIST 100L
#define SEC_TO_MICRO 1000000L
#define ERROR_PERCENT 2
#define DISTANCE_BUFFER_SIZE 8


/*********************************************************
  function name:        newTransportDistanceTask
  function inputs:      none
  function outputs:     new TCB for transportDistance
  function description: create an transport distance TCB with
             its dataPtr initialized.
  author:               Khang Phan
***********************************************************/
TCB newTransportDistanceTask() {
  TCB emptyTCB;

  static struct transportDistanceDataStruct transportDistanceData;
  transportDistanceData.transportDistancePointerPtr = &transportDistancePointer;
  emptyTCB.taskPtr = (void*) transportDistanceFunc;
  emptyTCB.taskDataPtr = &transportDistanceData;
  emptyTCB.priority = 5;
  return emptyTCB;
}

/************************************************************
  function name:        transportDistanceFunc
  function inputs:      struct transportDistanceDataStruct *dataPtr
  function outputs:     none
  function description: main function of transport distance TCB,
            read the distance between the transport vehicle
            and satellite
  author:               Khang Phan
*************************************************************/
void transportDistanceFunc (struct transportDistanceDataStruct *dataPtr) {
  struct TransportDistancePointerStruct* distance = (dataPtr->transportDistancePointerPtr);
  int newDistance = getDistanceMeter();
  // if new distance reading < 0 that mean there is an error reading
  // or the transport vehicle is too far
  if (newDistance > 0) {
    // Get last reading value
    int oldDistance = distance->transportDistance[distance->front];
    int different = newDistance - oldDistance;
    if (different < 0) {
      different = -different;
    }

    // Update if change more than 10%
    if (different / (double) oldDistance >= 0.1) {
      distance->front += 1;
      distance->front %= 8;
      distance->transportDistance[distance->front] = newDistance;
    }
  }

  return;
}

/************************************************************
  function name:        getDistanceMeter
  function inputs:      none
  function outputs:     int
  function description: read distance, return it, return -1
               if there are errors or transport vehicle is too
               far
  author:               Khang Phan
*************************************************************/
int getDistanceMeter() {
  if (successTransitionTime < prevSuccessTransitionTime) {
    return -1;
  }

  // Get the period from transiton times from high to low
  unsigned long period = successTransitionTime - prevSuccessTransitionTime;
  // convert to frequency
  unsigned long frequency = SEC_TO_MICRO / period;

  // Return distance base on frequency
  if (frequency > MAX_FREQ) {
    return MIN_DIST;
  }
  if (frequency < MIN_FREQ) {
    if (frequency < (MIN_FREQ  * (1.0 - ((double)ERROR_PERCENT) / 100.0))) {
      return -1;
    }
    return MAX_DIST;
  }
  return (int)((MAX_DIST - MIN_DIST) * (MAX_FREQ - frequency) / (MAX_FREQ - MIN_FREQ) + MIN_DIST);

}

/*******************************************************
  destructSubsystem.h
  Authors: John McIntyre
  Assignment 5
  12/09/18
  This is header of destructSubsystem.c, containing the data
  struct used by distructSubsystem
*********************************************************/
#ifndef DESTRUCT_SUBSYS_H
#define DESTRUCT_SUBSYS_H

#include <Arduino.h>
#include <Elegoo_TFTLCD.h>

extern Elegoo_TFTLCD tft;

#include "Arduino_Final.h"

#include "globalVariableInitialization.h"
#include "TCBStruct.h"

extern unsigned long millisec;
struct destructSubsystemDataStruct {
  //Bool *clearDestructButton;
  unsigned short *timeToDestructPtr;

};

void destructSubsystemFunc (struct destructSubsystemDataStruct *dataPtr);
TCB newDestructSubsystemTask();

void flashScreen(void);
void destructProtocol(void);
void decreTimeToDestruct(void);
void printDestruct();

extern unsigned long destructFlashTimer;
extern unsigned long destructCounterTimer;
extern Bool clearDestructButton;



#endif

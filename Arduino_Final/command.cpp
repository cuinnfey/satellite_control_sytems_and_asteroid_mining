/*******************************************************
* command.cpp
* Authors: Khang Phan
* Assignment 5
* 12/10/18
* This program handle command from Earth
*********************************************************/
#include "command.h"

/*********************************************************
* function name:        newCommandSubsystemTask
* function inputs:      none
* function outputs:     new TCB for command
* function description: create a battery command subsystem 
*           TCB with its dataPtr initialized.
* author:               Khang Phan
***********************************************************/
TCB newCommandSubsystemTask() {
  TCB emptyTCB;

  static struct commandDataStruct commandData;

  commandData.batteryOverTemperaturePtr = &batteryOverTemperature;
  commandData.fuelLowPtr = &fuelLow;
  commandData.batteryLowPtr = &batteryLow;
  commandData.solarPanelStatePtr = &solarPanelState;
  commandData.batteryLevelPtr = &batteryLevel;
  commandData.fuelLevelPtr = &fuelLevel;
  commandData.powerConsumptionPtr = &powerConsumption;
  commandData.powerGenerationPtr = &powerGeneration;
  commandData.transportDistancePointerPtr = &transportDistancePointer;
  commandData.batteryTemperaturePointerPtr = &batteryTemperaturePointer;
  commandData.imageBufferPtr = &imageBuffer;
  commandData.thrusterCommandPtr = &thrusterCommand;


  emptyTCB.taskPtr = (void (*)(void*)) commandFunc;
  emptyTCB.taskDataPtr = &commandData;
  emptyTCB.priority = 1;
  return emptyTCB;
}

/************************************************************
* function name:        commandFunc
* function inputs:      struct commandDataStruct *dataPtr
* function outputs:     none
* function description: main function of battery temperature TCB,
*           measuring battery, handle command and response from
*           and to Earth
* author:               Khang Phan
*************************************************************/
void commandFunc(struct commandDataStruct *dataPtr) {
  String command = fromEarth;
  fromEarth = NULL_STRING;
  command.toUpperCase();
  int len = command.length();
  if (len == 1) {
    if (command == "S") {
      taskStart();
      toEarth = "A";
      return;
    } else if (command == "P") {
      taskStop();
      toEarth = "A";
      return;
    } else if (command == "D") {
      if (remoteDisplay) {
        // turn on remote display
        remoteDisplay = FALSE;
      } else {
        // turn off remote display
        remoteDisplay = TRUE;
      }
      toEarth = "A";
      return;
    }
  } else if (len > 1) {
    if (command.charAt(0) == 'M') {
      request(command.substring(1), dataPtr);
      return;
    } else if (command.charAt(0) == 'T') {
      setThruster(command.substring(1), dataPtr->thrusterCommandPtr);
      return;
    }
  }
  toEarth = "E";
}

/************************************************************
* function name:        taskStart
* function inputs:      none
* function outputs:     none
* function description: corresponding to command S, in this case,
*           turn on pirate detecting system
* author:               Khang Phan
*************************************************************/
void taskStart() {
  pirateDistanceActivated = TRUE;
}

/************************************************************
* function name:        taskStop
* function inputs:      none
* function outputs:     none
* function description: corresponding to command P, in this case,
*           turn off pirate detecting system
* author:               Khang Phan
*************************************************************/
void taskStop() {
  pirateDistanceActivated = FALSE;
}


/************************************************************
* function name:        taskStop
* function inputs:      String requested , struct commandDataStruct *dataPtr
* function outputs:     none
* function description: corresponding to command M, parse
*           data to know which data is requested and set
*           string so satellite can send data back to Earth.
*           If request data is not recognized, send back error.
* author:               Khang Phan
*************************************************************/
void request(String requested , struct commandDataStruct *dataPtr) {
  if (requested == "BATTERY_LEVEL") {
    toEarth = "M" + String(*(dataPtr->batteryLevelPtr));
  } else if (requested == "FUEL_LOW") {
    toEarth = "M" + String(*(dataPtr->fuelLowPtr));
  } else if (requested == "BATTERY_LOW") {
    toEarth = "M" + String(*(dataPtr->batteryLowPtr));
  } else if (requested == "SOLAR_PANEL_STATE") {
    toEarth = "M" + String(*(dataPtr->solarPanelStatePtr));
  } else if (requested == "FUEL_LEVEL") {
    toEarth = "M" + String(*(dataPtr->fuelLevelPtr));
  } else if (requested == "POWER_CONSUMPTION") {
    toEarth = "M" + String(*(dataPtr->powerConsumptionPtr));
  } else if (requested == "POWER_GENERATION") {
    toEarth = "M" + String(*(dataPtr->powerGenerationPtr));
  } else if (requested == "TRANSPORT_DISTANCE") {
    toEarth = "M" + String(dataPtr->transportDistancePointerPtr->transportDistance[dataPtr->transportDistancePointerPtr->front]);
  } else if (requested == "BATTERY_TEMPERATURE") {
    toEarth = "M" + String(millivoltToCelcius(dataPtr->batteryTemperaturePointerPtr->temperatureMillivolt1[dataPtr->batteryTemperaturePointerPtr->front])) + ' '
              + String(millivoltToCelcius(dataPtr->batteryTemperaturePointerPtr->temperatureMillivolt2[dataPtr->batteryTemperaturePointerPtr->front]));
  } else if (requested == "IMAGE_DATA") {
    toEarth = "M" + String(dataPtr->imageBufferPtr->bufferArray[imageBuffer.front]);
  } else {
    toEarth = "E";
  }
}


/************************************************************
* function name:        setThruster
* function inputs:      String code, unsigned int* thrusterCommandPtr
* function outputs:     none
* function description: corresponding to command T, parse
*           data thruster command and set it to thruster if 
*           valid, if not send back error
* author:               Khang Phan
*************************************************************/
void setThruster(String code, unsigned int* thrusterCommandPtr) {
  unsigned int tempThruster = 0;
  if (code.length() == 16) {
    for (int i = 15; i >= 0; i--) {
      char c = code.charAt(i);
      switch (c) {
        case '1':
          tempThruster |= 1 << (15 - i);
        case'0':
          break;
        default:
          toEarth = "E";
          return;
      }
    }
    Serial.println("Thruster command changed to: " + code);
    *thrusterCommandPtr = tempThruster;
    toEarth = "A";
  } else {
    toEarth = "E";
    return;
  }
}

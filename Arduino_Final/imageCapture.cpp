/*******************************************************
  imageCapture.cpp
  Authors: Cuinn Fey
  Assignment 5
  12/5/18

*********************************************************/
#include "imageCapture.h"
#include "optfft.h"

#define SAMPLING_FREQ 8192UL
#define ONE_SEC_MICRO 1000000UL
#define SAMPLE_SIZE 256


unsigned long samplingPeriod = ONE_SEC_MICRO / SAMPLING_FREQ;

/*********************************************************
  function name:        newImageCaptureSubsystemTask
  function inputs:      none
  function outputs:     new TCB for imageCapture
  function description: create imageCapture TCB with
                         its dataPtr initialized.
  author:               Cuinn Fey
***********************************************************/
TCB newImageCaptureSubsystemTask() {
  TCB emptyTCB;

  static struct imageCaptureDataStruct imageCaptureData;

  imageCaptureData.vRealPtr  = (int*)&vReal;
  imageCaptureData.vImagPtr = (int*)&vImag;

  emptyTCB.taskPtr = (void (*)(void*))  imageCaptureFunc;
  emptyTCB.taskDataPtr = &imageCaptureData;
  emptyTCB.priority = 1;

  return emptyTCB;
}


/************************************************************
  function name:        imageCaptureFunc
  function inputs:      struct imageCaptureDataStruct *dataPtr
  function outputs:     none
  function description: main function of imageCapture. 
                        Perform FFT on raw image data and store
                        largest frequency value in a buffer
  author:               Cuinn Fey
*************************************************************/
void imageCaptureFunc(struct imageCaptureDataStruct *dataPtr)
{
  //initialize things
  Serial.println("Processing image");
  unsigned long f = 0;
  int m_index = -1;
  // initialize imaginary vector
  // Read data
  int i;
  for (i = 0; i < SAMPLE_SIZE; i++) {
    (dataPtr->vImagPtr)[i] = 0;
  }
  // Perform fft
  m_index = optfft(vReal, vImag);
  f = m_index * SAMPLING_FREQ / SAMPLE_SIZE;
  imageBuffer.front++;
  imageBuffer.front %= 16;
  imageBuffer.bufferArray[imageBuffer.front] = f;
  return;
}

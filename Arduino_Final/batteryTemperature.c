/*******************************************************
* batteryTemperature.c
* Authors: Khang Phan
* Assignment 4
* 11/15/18
* This program handles read the temperature of battery
*********************************************************/
#include "batteryTemperature.h"

#define MAX_TEMP_MILLIVOLT 325U
#define MAX_ANALOG_READ 655U
#define TEMP_SCALE 32U
#define TEMP_OFFSET 33U
#define OVER_TEMP 1.2

/*********************************************************
* function name:        newBatteryTempTask
* function inputs:      none
* function outputs:     new TCB for batteryTemperature
* function description: create a battery temperature reading 
*            subsystem TCB with its dataPtr initialized.
* author:               Khang Phan
***********************************************************/
TCB newBatteryTempTask() {
    TCB emptyTCB;

    static struct batteryTempStruct batteryTempData;

    batteryTempData.batteryTemperaturePointerPtr = &batteryTemperaturePointer;
    emptyTCB.taskPtr = (void*) batteryTempFunc;
    emptyTCB.taskDataPtr = &batteryTempData;
    emptyTCB.priority = 5;
    return emptyTCB;
}

/************************************************************
* function name:        batteryTempFunc
* function inputs:      struct batteryTempStruct *dataPtr
* function outputs:     none
* function description: main function of battery temperature TCB,
*           measuring battery temperature
* author:               Khang Phan
*************************************************************/
void batteryTempFunc (struct batteryTempStruct *dataPtr) {
    static unsigned int prevMaxMilliVolt = 0;
    // Update front index of buffer
    dataPtr->batteryTemperaturePointerPtr->front++;
    dataPtr->batteryTemperaturePointerPtr->front %= 16;

    // Analog read then convert it to millivolt
    unsigned int readingMilliVolt1 = (unsigned int) (analogRead(BATT_TEMP_PIN1)*(MAX_TEMP_MILLIVOLT/(double) MAX_ANALOG_READ));
    unsigned int readingMilliVolt2 = (unsigned int) (analogRead(BATT_TEMP_PIN2)*(MAX_TEMP_MILLIVOLT/(double) MAX_ANALOG_READ));

    // Set flag if over temperature, when either one of the two reading is 20% more than the 
    // maximum reading from last time.
    if (readingMilliVolt1 / (double) prevMaxMilliVolt >= OVER_TEMP || readingMilliVolt2 / (double) prevMaxMilliVolt >= OVER_TEMP) {
      batteryOverTemperature = TRUE;
    }

    // Update the maximum millivolt fromt last reading
    if (readingMilliVolt1 > readingMilliVolt2) {
      prevMaxMilliVolt = readingMilliVolt1;
    } else {
      prevMaxMilliVolt = readingMilliVolt2;
    }

    // Update buffer
    dataPtr->batteryTemperaturePointerPtr->temperatureMillivolt1[dataPtr->batteryTemperaturePointerPtr->front] = readingMilliVolt1;
    dataPtr->batteryTemperaturePointerPtr->temperatureMillivolt2[dataPtr->batteryTemperaturePointerPtr->front] = readingMilliVolt2;
    return;
}

/************************************************************
* function name:        batteryTempFunc
* function inputs:      unsigned int millivolt
* function outputs:     int
* function description: converting millivolt to temperature
*            in Celcius
* author:               Khang Phan
*************************************************************/
unsigned int millivoltToCelcius(unsigned int millivolt) {
  // Temperature = 32 * volt + 33
  return 32 * millivolt / 100 + 33;
}

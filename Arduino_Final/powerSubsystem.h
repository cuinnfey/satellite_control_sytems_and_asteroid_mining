/*******************************************************
* powerSubsystem.h
* Authors: Khang Phan
* Assignment 2
* 10/14/18
* This is header of powerSubsystem.c, containing the data
* struct used by consoleDisplay
*********************************************************/
#ifndef POWER_SUBSYS_H
#define POWER_SUBSYS_H

#include <Arduino.h>
#ifdef __cplusplus
extern "C" {
#endif

#include "globalVariableInitialization.h"
#include "TCBStruct.h"


struct powerSubsystemDataStruct {
    Bool *solarPanelStatePtr, *solarPanelDeployPtr, *solarPanelRetractPtr;
    unsigned short *batteryLevelPtr, *powerConsumptionPtr, *powerGenerationPtr;
    struct BatteryPointerStruct *batteryPointerPtr;
};

void powerSubsystemFunc (struct powerSubsystemDataStruct *dataPtr);
void updatePowerConsumption (unsigned short *powerConsPtr, Bool evenCalled);
void updatePowerGen (unsigned short *powerGenPtr, unsigned short *battLevelPtr, Bool *solarPanStatPtr, Bool evenCalled);
TCB newPowerSubsystemTask();
void connectMeasureEquipment();
void  disconnectMeasureEquipment();
void readBattery();
void readBatteryInterruptFunction();
#ifdef __cplusplus
} // extern "C"
#endif

extern TCB solarControlTCB;

extern Bool measureBatteryInterrupt;

#endif

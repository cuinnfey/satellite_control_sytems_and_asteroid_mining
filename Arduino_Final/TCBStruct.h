/*******************************************************
  TCBStruct.h
  Authors: Provided on class page
  Modified by Cuinn Fey
  Assignment 3
  This header declare the task prototype structure being
  used by the project as a doubly linked list
  Last Modified: October 21st, 2018
*********************************************************/
#ifndef TCB_STRUCT_H
#define TCB_STRUCT_H
//  Declare a TCB structure

typedef struct TCB
{
  void* taskDataPtr;
  void (*taskPtr)(void*);
  struct TCB* next; // Points to next task
  struct TCB* prev; // Points to previous task
  int priority;
} TCB;

#endif

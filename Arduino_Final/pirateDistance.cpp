/*******************************************************
  pirateDistance.c
  Authors: Cuinn Fey
  Assignment 5
  12/5/18

*********************************************************/
#include "pirateDistance.h"

#define MAX_FREQ 80L
#define MIN_FREQ 4L
#define MAX_DIST 200L
#define MIN_DIST -10L
#define SEC_TO_MICRO 1000000L
#define ERROR_PERCENT 2
#define DISTANCE_BUFFER_SIZE 8


/*********************************************************
  function name:        newPirateDistanceTask
  function inputs:      none
  function outputs:     new TCB for pirateDistance
  function description: create an pirate distance TCB with
                         its dataPtr initialized.
  author:               Cuinn Fey
***********************************************************/
TCB newPirateDistanceTask() {
  TCB emptyTCB;
  static struct pirateDistanceDataStruct pirateDistanceData;

  pirateDistanceData.pirateDistanceMeterPtr = &pirateDistanceMeter;
  pirateDistanceData.pirateDetectedPtr = &pirateDetected;
  emptyTCB.taskPtr = (void*) pirateDistanceFunc;
  emptyTCB.taskDataPtr = &pirateDistanceData;
  emptyTCB.priority = 5;
  return emptyTCB;
}

/************************************************************
  function name:        pirateDistanceFunc
  function inputs:      struct pirateDistanceDataStruct *dataPtr
  function outputs:     none
  function description: main function of pirateDistance
  author:               Cuinn Fey
*************************************************************/
void pirateDistanceFunc (struct pirateDistanceDataStruct *dataPtr) {
  int newDistance = getDistancePirateMeter();
  if (newDistance < 0) {
    newDistance = 0;
  }
  static int oldDistance = newDistance;
  int difference = newDistance - oldDistance;
  if (difference < 10 && difference > -10) {
    if (newDistance >= 0 && newDistance <= 100) {
      *(dataPtr->pirateDetectedPtr) = TRUE;
    } else {
      *(dataPtr->pirateDetectedPtr) = FALSE;
    }
    * (dataPtr->pirateDistanceMeterPtr) = newDistance;
  }
  oldDistance = newDistance;
  return;
}

/************************************************************
  function name:        getDistancePirateMeter
  function inputs:      none
  function outputs:     none
  function description: read distance, return it, return -1
               if there are errors or transport vehicle is too
               far
  author:               Khang Phan
*************************************************************/
int getDistancePirateMeter() {
  if (pirateSuccessTransitionTime < prevPirateSuccessTransitionTime) {
    return -1;
  }

  unsigned long period = pirateSuccessTransitionTime - prevPirateSuccessTransitionTime;

  unsigned long frequency = SEC_TO_MICRO / period;
  if (frequency > MAX_FREQ) {
    return MIN_DIST;
  }
  if (frequency < MIN_FREQ) {
    if (frequency < (MIN_FREQ  * (1.0 - ((double)ERROR_PERCENT) / 100.0))) {
      return -1;
    }
    return MAX_DIST;
  }
  int retVal = (int)((MAX_DIST - MIN_DIST) * (MAX_FREQ - frequency) / (MAX_FREQ - MIN_FREQ) + MIN_DIST);
  if (retVal < 0) {
    return 0;
  } else {
    return retVal;
  }
}

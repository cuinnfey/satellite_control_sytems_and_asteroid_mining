/*******************************************************
* scheduler.h
* Authors: Khang Phan
* Assignment 2
* 10/14/18
* This is header of scheduler.c, connecting to the global
* timer/counter millisec
*********************************************************/
#ifndef SCHEDULER_H
#define SCHEDULER_H
#include "Arduino_Final.h"
extern unsigned long millisec;
extern int solarInputMode;
extern void clearDestruct();
extern int readDestructCode();

#ifdef __cplusplus
extern "C" {
#endif

#include "globalVariableInitialization.h"
#include "taskQueue.h"
#include "TCBStruct.h"


void doNextTask();
void initializeScheduler();
#ifdef __cplusplus
} // extern "C"
#endif

#endif
